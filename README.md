mvRDF2vec
============

This repository contains the source code to use the medicalvalues knowledge graph to predict relations between risk factors (e.g. laboratory parameters) and diseases.

The repository is split into two parts: The rdf directory that is written in Node.js. It can be used to transform the medicalvalues labeled property graph into an RDF graph. The classification directory contains python code to build ML models that work based on vector embeddings.
