from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split, GridSearchCV
from get_input import get_signals_input
from evaluate_model import evaluate_models
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression


features, labels = get_signals_input()

train_features, test_features, train_labels, test_labels = train_test_split(features, labels, 
                                                                            test_size = 0.25, random_state = 42, stratify=labels)

random_state = 42
folds = 5
scoring = 'accuracy'

print("Total training input: " + str(len(train_labels)))

rf = RandomForestClassifier(random_state = random_state)

# Number of trees in random forest
n_estimators = [50, 100, 150]
# Number of features to consider at every split
max_features = ['auto', 'sqrt']
# Maximum number of levels in tree
max_depth = [50, 100, 300, None]
# Minimum number of samples required to split a node
min_samples_split = [2, 5, 10]
# Minimum number of samples required at each leaf node
min_samples_leaf = [1, 2, 4]
# Method of selecting samples for training each tree
bootstrap = [True, False]
# Create the random grid
random_forest_grid = {'n_estimators': n_estimators,
               'max_features': max_features,
               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf,
               'bootstrap': bootstrap}

print('Fit random forest')

rf = RandomForestClassifier(random_state = random_state)
rfc_clf = GridSearchCV(estimator=rf, param_grid=random_forest_grid, cv=folds, n_jobs=-1, scoring=scoring)
rfc_clf.fit(train_features, train_labels)
best_rf_model = rfc_clf.best_estimator_

svm_grid = {
    'kernel': ('linear', 'rbf', 'poly', 'sigmoid'),
    'C': [0.01, 1, 10, 100]
} 



print('Fit SVM')

svc = SVC(gamma='auto')

svc_clf = GridSearchCV(estimator=svc, param_grid=svm_grid, cv=folds, n_jobs=-1, scoring=scoring)
svc_clf.fit(train_features, train_labels)

best_svm_model = svc_clf.best_estimator_

print('Logistic Regression')
# define models and parameters
model = LogisticRegression()
solvers = ['newton-cg', 'lbfgs', 'liblinear']
penalty = ['l2']
c_values = [100, 10, 1.0, 0.1, 0.01]
# define grid search
log_grid = dict(solver=solvers,penalty=penalty,C=c_values)

log_reg = LogisticRegression(random_state=random_state, max_iter=10000)

log_clf = GridSearchCV(estimator=log_reg, param_grid=log_grid, cv=folds, n_jobs=-1, scoring=scoring)
log_clf.fit(train_features, train_labels)
best_log_model = log_clf.best_estimator_

evaluate_models(best_log_model, best_svm_model, best_rf_model, test_features, test_labels)

from save_model import save

save(best_rf_model)