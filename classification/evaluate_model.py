from sklearn.metrics import roc_curve, roc_auc_score, f1_score, precision_score, recall_score, accuracy_score
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix, classification_report, ConfusionMatrixDisplay


def evaluate(y_test, y_pred):
    f1 = f1_score(y_test, y_pred)
    print("F1 Score: " + str(f1))
    false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, y_pred)
    roc_auc = roc_auc_score(false_positive_rate, true_positive_rate)
    precision = precision_score(y_test, y_pred)
    recall = recall_score(y_test, y_pred)
    print("Precision: " + str(precision))
    print("Recall: " + str(recall))
    print("AUC: " + str(roc_auc))
    plt.figure()
    lw = 2
    plt.plot(false_positive_rate, true_positive_rate, color='darkorange', lw=lw, label='ROC curve (area = %0.2f)' % roc_auc)
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()

def evaluate_model(model, x_test_features, y_test, name):
    y_pred = model.predict(x_test_features)
    f1 = f1_score(y_test, y_pred)
    roc_auc = roc_auc_score(y_test, y_pred)
    precision = precision_score(y_test, y_pred)
    recall = recall_score(y_test, y_pred)
    accuracy = accuracy_score(y_test, y_pred)
    print("Accuracy: " + str(accuracy))
    print("Precision: " + str(precision))
    print("Recall: " + str(recall))
    print("F1 Score: " + str(f1))
    print("AUC: " + str(roc_auc))
    print(confusion_matrix(y_test, y_pred))
    ConfusionMatrixDisplay.from_estimator(model, x_test_features, y_test, display_labels=["negative", "positive"], cmap="YlOrRd")
    plt.savefig("plots/cnf_" + name + ".png", dpi=500)
    return y_pred, roc_auc

def evaluate_models(logreg, svm, rf, x_test_features, y_test):
    print("############# LogReg ################")
    logreg_pred, logreg_auc = evaluate_model(logreg, x_test_features, y_test, "logreg")
    print("############   SVM   ################")
    svm_pred, svm_auc = evaluate_model(svm, x_test_features, y_test, "svm")
    print("############    RF   ################")
    rf_pred, rf_auc = evaluate_model(rf, x_test_features, y_test, "rf")
    fig, ax = plt.subplots()
    fpr, tpr, _ = roc_curve((y_test), (logreg_pred))
    ax.plot(fpr, tpr, lw=2, label='Logistic Regression (AUC = %0.2f)' % logreg_auc)
    fpr, tpr, _ = roc_curve(y_test, svm_pred)
    ax.plot(fpr, tpr, lw=2, label='Support Vector Machines (AUC = %0.2f)' % svm_auc)
    fpr, tpr, _ = roc_curve(y_test, rf_pred)
    ax.plot(fpr, tpr, lw=2, label='Random Forest (AUC = %0.2f)' % rf_auc)
    ax.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')
    ax.legend(loc=4)
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Postive Rate')
    fig.savefig("plots/roc.png", dpi=500)



