import pandas
import numpy as np

from config import config

def get_rule_names(id):
    df = pandas.read_csv(config["input"]["rules"], delimiter="|")
    rules = df.loc[df['id'] == id]
    return rules['name'].values.tolist()

def get_vector(df, name):
    return df.loc[df['name'] == name]['vectors'].values[0]

def get_risk_factors():
    df = pandas.read_csv(config["input"]["risk_factors"], delimiter="|")
    return df.values.tolist()

def get_risk_factor_names():
    df = pandas.read_csv(config["input"]["risk_factors"], delimiter="|")
    return df["name"].values.tolist()


def get_rules():
    df = pandas.read_csv(config["input"]["rules"], delimiter='|')
    rules = df['name'].values.tolist()
    return rules

def get_signals_input():
    data = np.loadtxt(config['output']['signals_x'], delimiter=config['output']['delimiter'])
    target = np.loadtxt(config['output']['signals_y'], delimiter=config['output']['delimiter'])
    return data, target

def extract(path_to_file):
    result = []
    with open(path_to_file) as f:
        for line in f:
            toArray = line.split()
            description = toArray[0]
            vectorStrings = toArray[1:]
            vectors = []
            for vectorString in vectorStrings:
                vectors.append(float(vectorString))
            resultElement = [description, vectors]
            result.append(resultElement)
    return pandas.DataFrame(result, columns=['name', 'vectors'])