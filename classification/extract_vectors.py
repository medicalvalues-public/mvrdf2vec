import pandas as pd

def extract(path_to_file):
    result = []
    with open(path_to_file) as f:
        for line in f:
            toArray = line.split()
            description = toArray[0]
            vectorStrings = toArray[1:]
            vectors = []
            for vectorString in vectorStrings:
                vectors.append(float(vectorString))
            resultElement = [description, vectors]
            result.append(resultElement)
    return pd.DataFrame(result, columns=['name', 'vectors'])
    
