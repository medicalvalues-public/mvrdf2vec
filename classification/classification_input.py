from random import randint
from numpy import savetxt
import pandas

from extract_vectors import extract
from get_input import get_risk_factor_names, get_risk_factors, get_rule_names, get_vector, get_rules
from config import config


def is_in_existing_relations(fromDescription, toDescription, existingRelations):
    for relation in existingRelations:
        if relation[0] == fromDescription and relation[1] == toDescription:
            return True
    return False

def randomRelations(existing_relations, needed_relation_count, vectors):
    result = []
    rules = get_rules()
    risk_factors = get_risk_factor_names()
    rules_length = len(rules) - 1
    risk_factors_length = len(risk_factors) - 1
    negative_labels = []
    for i in range(0, needed_relation_count):
        negative_labels.append(0)
        found = False
        while not found:
            risk_factor_random = randint(0, risk_factors_length)
            rule_random = randint(0, rules_length)
            factor = risk_factors[risk_factor_random]
            rule = rules[rule_random]
            if not is_in_existing_relations(factor, rule, existing_relations):
                found_factor, vector_factor = matchVector(vectors, factor) 
                found_rule, vector_rule = matchVector(vectors, rule)
                if found_factor & found_rule:
                    result_vector = vector_factor + vector_rule
                    result.append(result_vector)
                    found = True

    return result, negative_labels

def biased_negative_labes(existing_relations, needed_relation_count, vectors): 
    result = []
    rules = get_rules()
    risk_factors = get_risk_factor_names()
    risk_factors_length = len(risk_factors) - 1
    negative_labels = []
    print(str(len(rules)))
    for rule in rules:
        for i in range(0, config['input_generation']['negative_labels_per_rule']):
            negative_labels.append(0)
            found = False
            while not found:
                risk_factor_random = randint(0, risk_factors_length)
                factor = risk_factors[risk_factor_random]
                if not is_in_existing_relations(factor, rule, existing_relations):
                    found_factor, vector_factor = matchVector(vectors, factor) 
                    found_rule, vector_rule = matchVector(vectors, rule)
                    if found_factor & found_rule:
                        result_vector = vector_factor + vector_rule
                        result.append(result_vector)
                        found = True
    return result, negative_labels

def best_negative_labels(existing_relations, needed_relation_count, vectors):
    result = []
    negative_labels = []
    for relation in existing_relations:
        risk_factor = relation[0]
        found_rule, vector_rule = matchVector(vectors, relation[1])
        evaluation = risk_factor.split('_')
        index = len(evaluation)
        parameter = ""
        if evaluation[len(evaluation) - 2] != 'not':
            type = evaluation[len(evaluation) - 1]
            for i in range(0, index - 1):
                if i == index - 2:
                    parameter += evaluation[i]
                else:
                    parameter += evaluation[i] + '_' 
        else:
            type = evaluation[index - 2] + '_' + evaluation[index - 1]
            for i in range(0, index - 2):
                if i == index - 3:
                    parameter += evaluation[i]
                else:
                    parameter += evaluation[i] + '_' 
        opposite = {
            'yes': 'no',
            'no': 'yes',
            'not_normal': 'normal',
            'normal': 'not_normal',
            'not_decreased': 'decreased',
            'not_increased': 'increased',
            'decreased': 'increased',
            'increased': 'decreased'
        }.get(type, 'error')
        if opposite != 'error':
            opposite_text = parameter + '_' + opposite
            found_factor, vector_factor = matchVector(vectors, opposite_text)
            if found_factor and found_rule:
                if not is_in_existing_relations(opposite_text, relation[1], existing_relations):
                    result_vector = vector_factor + vector_rule
                    negative_labels.append(0)
                    result.append(result_vector)
                else:
                    print("Opposite relation is in rule")
            else:
                
                print("Opposite vector not found")
        else:
            print(type)
    return result, negative_labels

def best_negative_labels_variation(existing_relations, needed_relation_count, vectors):
    result = []
    negative_labels = []
    for relation in existing_relations:
        risk_factor = relation[0]
        found_rule, vector_rule = matchVector(vectors, relation[1])
        evaluation = risk_factor.split('_')
        index = len(evaluation)
        parameter = ""
        if evaluation[len(evaluation) - 2] != 'not':
            type = evaluation[len(evaluation) - 1]
            for i in range(0, index - 1):
                if i == index - 2:
                    parameter += evaluation[i]
                else:
                    parameter += evaluation[i] + '_' 
        else:
            type = evaluation[index - 2] + '_' + evaluation[index - 1]
            for i in range(0, index - 2):
                if i == index - 3:
                    parameter += evaluation[i]
                else:
                    parameter += evaluation[i] + '_' 
        opposite = {
            'yes': ['no'],
            'no': ['yes'],
            'not_normal': ['normal'],
            'normal': ['not_normal', 'increased', 'decreased'],
            'not_decreased': ['decreased'],
            'not_increased': ['increased'],
            'decreased': ['increased', 'normal'],
            'increased': ['decreased', 'normal']
        }.get(type, 'error')
        if opposite != 'error':
            for opposite_type in opposite:
                opposite_text = parameter + '_' + opposite_type
                found_factor, vector_factor = matchVector(vectors, opposite_text)
                if found_factor and found_rule:
                    if not is_in_existing_relations(opposite_text, relation[1], existing_relations):
                        result_vector = vector_factor + vector_rule
                        negative_labels.append(0)
                        result.append(result_vector)
                        break
                    else:
                        print("Opposite relation is in rule")
                else:
                    print("Opposite vector not found")
        else:
            print(type)
    return result, negative_labels

def matchVector(vectors, description):
    found = True
    vector = []
    try:
        vector = vectors[vectors.name == description].filter(items=['vectors']).values.tolist()[0][0]
    except IndexError:
        print('Not found for: ' + description)
        found = False
    return found, vector

def getVectors(relations, vectors):
    result = []
    total_not_found = 0
    for relation in relations:
        try:
            vector_from = vectors[vectors.name == relation[0]].filter(items=['vectors']).values.tolist()[0][0]
            vector_to = vectors[vectors.name == relation[1]].filter(items=['vectors']).values.tolist()[0][0]
            vector = vector_from + vector_to
            result.append(vector)
        except IndexError:
            total_not_found += 1
            print('Vector not found: ' + relation[0] + ', ' + relation[1])
    print('Total vectors not found:' + str(total_not_found))
    return result

def get_categories():
    df = pandas.read_csv(config["input"]["existing_relations"], delimiter="|")
    signals = df[df.type == 'SIGNALS']
    signals = signals.filter(items= ['risk_factor', 'rule']).values.tolist()
    required = df[df.type == 'REQUIRES']
    required = required.filter(items=['risk_factor', 'rule']).values.tolist()
    restricts = df[df.type == 'RESTRICTS']
    restricts = restricts.filter(items=['risk_factor', 'rule']).values.tolist()
    return signals, required, restricts

def get_positive_labels(list):
    labels = []
    for i in range(0, len(list)):
        labels.append(1)
    return labels

def get_input_data():
    print("Start extracting relations")
    print("Get vectors")
    word2_vec_vectors = extract(config["input"]["word2vec_output"])
    print("Load existing relations")
    signals, requires, restricts = get_categories()

    # get word2vec vectors
    signals_input = getVectors(signals, word2_vec_vectors)
    requires_input = getVectors(requires, word2_vec_vectors)
    restricts_input = getVectors(restricts, word2_vec_vectors)

    # positive labels
    signals_labels = get_positive_labels(signals_input)
    requires_labels = get_positive_labels(requires_input)
    restricts_labels = get_positive_labels(restricts_input)

    #negative_label_function = best_negative_labels_variation
    #negative_label_function = best_negative_labels
    negative_label_function = randomRelations
    if not (config['input_generation']['random_non_existing_relations']):
        negative_label_function = biased_negative_labes
    
    # build negative samples and their labels
    signals_random, signals_random_labels = negative_label_function(signals, len(signals_input), word2_vec_vectors)
    restricts_random, restricts_random_labels = negative_label_function(restricts, len(restricts_input), word2_vec_vectors)
    requires_random, requires_random_labels = negative_label_function(requires, len(requires_input), word2_vec_vectors)

    # put together relations
    all_signals_x = signals_input + signals_random
    print("Length Signals Negative Labels: " + str(len(signals_random)))
    print("Length Signals Positive Labels: " + str(len(signals_input)))
    all_signals_y =  signals_labels + signals_random_labels
    all_restricts_x = restricts_input + restricts_random
    all_restricts_y = restricts_labels + restricts_random_labels
    all_requires_x = requires_input + requires_random
    all_requires_y = requires_labels + requires_random_labels

    print("Total input of signals relations: " + str(len(all_signals_x)))
    savetxt(config["output"]["signals_x"], all_signals_x, delimiter=',')
    savetxt(config["output"]["signals_y"], all_signals_y, delimiter=',')
    print("Total input of restricts relations: " + str(len(all_restricts_x)))
    savetxt(config["output"]["restricts_x"], all_restricts_x, delimiter=',')
    savetxt(config["output"]["restricts_y"], all_restricts_y, delimiter=',')
    print('Total input of requires relations: ' + str(len(all_requires_x)))
    savetxt(config["output"]["requires_x"], all_requires_x, delimiter=',')
    savetxt(config["output"]["requires_y"], all_requires_y, delimiter=',')

get_input_data()
