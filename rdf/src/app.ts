import {Graph} from "./graph/graph";
import {LoadMvGraph} from "./mvGraphConnector/loadMvGraph";
import {NTriples} from "./rdfExport/nTriples";
import {GetPatients} from "./patient/input/mv/getPatients";
import {ExtractPatients} from "./patient/input/mimic/extractPatients";
import {ExportProcessedPatients} from "./patient/exportProcessedPatients";
import config from "./config";
import {EvaluatePatientsFromFile} from "./patient/evaluatePatientsFromFile";
import {RdfModel} from "./graph/rdfModel";
import {AddArtificialPatient} from "./patient/addArtificialPatient";
import {PatientsRiskFactorExporter} from "./patient/patientsRiskFactorExporter";
import {StatsManager} from "./patient/stats/statsManager";
import 'reflect-metadata';
import {Container} from "typedi";
import {Logger} from "./services/logger";

const logger = Container.get(Logger);
async function main() {
    if (config.loadPatients) {
        if (config.patientsSource === 'testdata') {
            const get = new GetPatients(config.output.patientsInput);
            await get.all();
        } else if (config.patientsSource === 'mimic') {
            await new ExtractPatients(config.output.patientsInput).all();
        } else {
            // implement other data sources here
            throw new Error('Data source not available yet');
        }
    }
    if (config.evaluatePatients) {
        const evaluate = new EvaluatePatientsFromFile(config.output.patientsInput, config.output.processedPatients);
        await evaluate.start();
    }
    if (config.buildRdfGraph) {
        let graph;
        const statsManager = new StatsManager(config.output.statsDirectory);
        const exportConfig = {
            riskFactorInPatientDataNeeded: config.graphOptions.exportRiskFactorWhenInData,
            minFactorCount: config.graphOptions.minFactorCount
        }
        if (config.rdfModel === 'singleFactorRule') {
            graph = new Graph(RdfModel.SingleFactorRule, statsManager, exportConfig);
        } else if (config.rdfModel === 'allFactorsRule') {
            graph = new Graph(RdfModel.AllFactorsRule, statsManager, exportConfig);
        } else {
            throw new Error('Unknown RDF model specified');
        }
        const loadMvg = new LoadMvGraph(graph);
        await loadMvg.exec(config.graphOptions.examination, config.graphOptions.laboratory, config.graphOptions.imaging);
        const exporter = new NTriples(config.output.graph);
        logger.log('Export basic graph');
        await graph.export(exporter);
        if (config.graphOptions.useEvaluatedPatients) {
            const exportPatientsToGraph = new ExportProcessedPatients(config.output.processedPatients);
            const patientRiskFactorExporter = new PatientsRiskFactorExporter(config.output.patientRiskFactors);
            await exportPatientsToGraph.exec(exporter, patientRiskFactorExporter, graph);
        }
        if (config.outputOptions.buildRiskFactors) {
            await graph.riskFactors.export(config.output.riskFactors);
        }
        if (config.outputOptions.buildRiskRelations) {
            await graph.exportRiskRelations(config.output.riskRelations);
        }
        if (config.graphOptions.addPatients) {
            const addPatients = new AddArtificialPatient(exporter, config.output.addedPatients, graph);
            await addPatients.addByDisease(config.graphOptions.minDiseasePatientQuantity, config.graphOptions.addPatientQuantity,
                config.output.statsDirectory + '/diseases.csv');
        }
    }
}

main();
