export default {
    knowledgeGraphAPI: 'http://127.0.0.1:5000/graphql',

    reasoningUrl: 'http://127.0.0.1:2000/machine-learning/',

    // postgres connection to the mimic_iv database
    postgres: {
        user: 'postgres',
        host: 'localhost',
        database: 'postgres',
        password: 'postgres',
        port: 5432
    },

    mimicOptions: {
        // possible types are 'AMBULATORY OBSERVATION', 'DIRECT EMER.', 'DIRECT OBSERVATION', 'ELECTIVE', 'EW EMER.',
        // 'EU OBSERVATION', 'OBSERVATION ADMIT', 'SURGICAL SAME DAY ADMISSION', 'URGENT'
        admissionTypes: ['AMBULATORY OBSERVATION', 'DIRECT EMER.', 'DIRECT OBSERVATION', 'ELECTIVE', 'EU OBSERVATION',
            'OBSERVATION ADMIT', 'URGENT'],

        // false: extract admission only if patient did not die in admission
        diedPatients: false,

        // extract only admissions that have the same amount or more parameters that could be match with mv graph
        minCountParameters: 5
    },

    graphOptions: {
        // use examination rules
        examination: false,
        // use laboratory rule
        laboratory: true,
        // use imaging rules
        imaging: false,
        // add evaluated patients to the RDF graph
        useEvaluatedPatients: true,
        // add extra patients to the graph
        addPatients: false,
        // number of patients per disease needed to add extra patients
        minDiseasePatientQuantity: 5,
        // number of patients that are added per disease
        addPatientQuantity: 1,
        // only export when available in patients data
        exportRiskFactorWhenInData: true,
        // count of how often the risk factor must be available
        minFactorCount: 5
    },

    outputOptions: {
        buildRiskFactors: true,
        buildRiskRelations: true
    },

    // the status of the path, in_creation, in_optimization, in_review, productive
    pathStatus: ['in_creation', 'in_optimization', 'in_review', 'productive'],

    // load patients yes or no
    loadPatients: false,

    // source from where patients should be loaded either 'mimic' or 'testdata'
    patientsSource: 'mimic',

    // evaluate patients yes or no
    evaluatePatients: true,

    // build and export rdf graph
    buildRdfGraph: true,

    // either singleFactorRule or allFactorsRule
    rdfModel: 'allFactorsRule',

    output: {
        patientsInput: 'data/patients.csv',
        processedPatients: 'data/evaluated_patients.csv',
        statsDirectory: 'data/stats',
        graph: 'data/graph.nt',
        riskFactors: 'data/risk_factors.csv',
        riskRelations: 'data/risk_relations.csv',
        patientRiskFactors: 'data/patient_risk_relations.csv',
        addedPatients: 'data/patient_added.csv'
    }
}
