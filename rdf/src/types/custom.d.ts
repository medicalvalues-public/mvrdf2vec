export type additionalParameter = {
    description: string;
    importance?: number;
    core?: boolean;
}

export type Patient = {
    id: string;
    gender: string;
    age: number | string;
    ageUnit: string;
    ethnicity?: string;
}

export type ImagingResult = {
    datetime: string;
    snomed: string;
    hasImagingResult: boolean;
    description?: string;
}

export type Parameter = {
    loinc: string | null;
    snomed: string | null;
    value: number | string;
    unit: string;
    description?: string;
    referenceRange?: ReferenceRange;
    datetime?: string;
}

export type Finding = {
    snomed: string;
    hasFinding: boolean;
    description?: string;
    datetime?: string;
}

export type Disease = {
    description?: string;
    icd10?: string;
    snomed: string;
}


export type ReferenceRange = {
    gender: string;
    unit: string;
    ageMin: number;
    ageMinUnit: string;
    ageMax: number;
    ageMaxUnit: string;
    valueMin: string;
    valueMax: string;
    source: Object;
}