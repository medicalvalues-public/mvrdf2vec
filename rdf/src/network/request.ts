import {request} from 'graphql-request'
import config from '../config';

export class Request {
    public static async exec(query: string) {
        let tries = 0;
        let sleep = 1000;
        while (true) {
            try {
                tries++;
                return await request(config.knowledgeGraphAPI, query);
            } catch (error) {
                if (tries === 1) {
                    console.log(error);
                }
                await new Promise(resolve => setTimeout(resolve, sleep));
                sleep = 2 * sleep;
                if (tries === 12) {
                    console.error(`Reasoning failed because of error loading data from postgraphile.`);
                    process.exit(1);
                }
            }
        }

    }
}
