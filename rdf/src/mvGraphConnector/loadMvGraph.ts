import {Request} from "../network/request";
import {QueryBuilder} from "./queryBuilder";
import {
    GetExaminationRule,
    GetImagingRule,
    GetLaboratoryRule,
    GetRule
} from "./models/rules";
import {Graph} from "../graph/graph";
import {RuleExporter} from "./ruleExporter";
import {RdfModel} from "../graph/rdfModel";
import {Disease} from "../graph/models/risk-factors/disease";
import config from "../config";
import {Logger} from "../services/logger";
import {Container} from "typedi";

export class LoadMvGraph {
    private readonly graph;
    private totalPaths = 0;
    private logger = Container.get(Logger);

    constructor(graph: Graph) {
        this.graph = graph;
    }

    public async exec(examination: boolean, laboratory: boolean, imaging: boolean) {
        this.logger.log('Load MV Graph via GraphQL API...');
        const ruleExporter = new RuleExporter('rules.csv');
        // Load paths according to their status
        for (const status of config.pathStatus) {
            this.logger.log('Load all paths with status ' + status + '.');
            const completeGraph = await Request.exec(QueryBuilder.getPathsPostgres(status));
            completeGraph.pathdata.nodes.forEach((pathData: any) => {
                this.getPath(pathData, examination, laboratory, imaging, ruleExporter);
            });
        }
        this.logger.log('Loading MV Graph completed.');
        this.logger.log('Total paths successfully processed: ' + this.totalPaths);
    }

    /*public async loadPathByDescription(description: string, examination: boolean, laboratory: boolean, imaging: boolean): Promise<void> {
        this.logger.log('Load disease path ' + description);
        const completeGraph = await Request.exec(QueryBuilder.getPathsPostgres());
        completeGraph.pathdata.nodes.forEach((pathData: any) => {
            if (pathData.description === description) {
                const ruleExporter = new RuleExporter('rules.csv');
                this.getPath(pathData, examination, laboratory, imaging, ruleExporter);
            }
        });
    }*/

    /**
     * Processes input from the medicalvalues graph
     * @param pathData
     * @param examination
     * @param laboratory
     * @param imaging
     * @param ruleExporter
     * @private
     */
    private getPath(pathData: any, examination: boolean, laboratory: boolean, imaging: boolean,
                    ruleExporter: RuleExporter): void {
        if (pathData.diseasesByPathdataId.nodes.length === 1) {
            const rawDisease = pathData.diseasesByPathdataId.nodes[0];
            if (rawDisease.icd10) {
                this.totalPaths++;
                const disease = this.graph.diseases.getOrCreate(rawDisease.icd10, rawDisease.description, rawDisease.id,
                    'currently_examined_with');
                if (this.graph.rdfModel === RdfModel.SingleFactorRule) {
                    this.multipleRules(pathData, examination, laboratory, imaging, ruleExporter, disease);
                } else if (this.graph.rdfModel === RdfModel.AllFactorsRule) {
                    this.singleRule(pathData, examination, laboratory, imaging, ruleExporter, disease);
                } else {
                    throw new Error('No RDF model specified.');
                }
            } else {
                this.logger.error('No ICD10 Code for disease ' + rawDisease.description);
            }
        } else if (pathData.diseasesByPathdataId.nodes.length > 1) {
            this.logger.error('More than one disease for path ' + pathData.description + '. Path will be ignored.');
        } else {
            this.logger.error('No disease for path ' + pathData.description + '. Path will be ignored.')
        }
    }

    private multipleRules(pathData: any, examination: boolean, laboratory: boolean, imaging: boolean,
                          ruleExporter: RuleExporter, disease: Disease): void {
        pathData.pathdataRulesByPathdataId.nodes.forEach((rule: any) => {
            let ruleNames: string[] = [];
            switch (rule.rule.type) {
                case 'ExaminationRule':
                    if (examination) {
                        ruleNames = GetExaminationRule.multipleRules(rule.rule, this.graph, ruleExporter);
                        this.addRulesToGraph(ruleNames, disease);
                    }
                    break;
                case 'LaboratoryRule':
                    if (laboratory) {
                        ruleNames = GetLaboratoryRule.multipleRules(rule.rule, this.graph, ruleExporter);
                        this.addRulesToGraph(ruleNames, disease);
                    }
                    break;
                case 'ImagingRule':
                    if (imaging) {
                        ruleNames = GetImagingRule.multipleRules(rule.rule, this.graph, ruleExporter);
                        this.addRulesToGraph(ruleNames, disease);
                    }
                    break;
            }
            if (ruleNames.length > 0) {
                GetRule.multipleRules(rule.rule, rule.weight, this.graph, ruleNames);
            }
        });
    }

    private singleRule(pathData: any, examination: boolean, laboratory: boolean, imaging: boolean,
                       ruleExporter: RuleExporter, disease: Disease): void {
        let ruleNames: Set<string> = new Set<string>();
        let rules: { name: string, riskFactors: { type: string, id: string } }[] = [];
        pathData.pathdataRulesByPathdataId.nodes.forEach((rule: any) => {
            if ((examination && rule.rule.type === 'ExaminationRule') || (laboratory && rule.rule.type === 'LaboratoryRule') ||
                (imaging && rule.rule.type === 'ImagingRule')) {

                const ruleName = GetRule.singleRule(rule.rule, this.graph, ruleExporter, rules);
                ruleNames.add(ruleName);
                switch (rule.rule.type) {
                    case 'ExaminationRule':
                        if (examination) {
                            GetExaminationRule.singleRule(rule.rule, this.graph, ruleName);
                        }
                        break;
                    case 'LaboratoryRule':
                        if (laboratory) {
                            GetLaboratoryRule.singleRule(rule.rule, this.graph, ruleName);
                        }
                        break;
                    case 'ImagingRule':
                        if (imaging) {
                            GetImagingRule.singleRule(rule.rule, this.graph, ruleName);
                        }
                        break;
                }
            }
        });
        const ruleNamesArray = Array.from(ruleNames);
        this.addRulesToGraph(ruleNamesArray, disease);

    }

    private addRulesToGraph(ruleNames: string[], disease: any): void {
        for (const ruleName of ruleNames) {
            const triple = disease.exportDiseaseRule(ruleName, 'DEFINES', this.graph);
            this.graph.triples.push(triple);
        }
    }
}
