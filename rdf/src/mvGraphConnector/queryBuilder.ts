import {pathQuery} from "./query";

export class QueryBuilder {
    public static getPathsPostgres(status: string): string {
        return `
            query loadPaths {
            pathdata(filter: {status: {equalTo: "${status}"}})
                ${pathQuery}   
        }`
            ;
    }
}
