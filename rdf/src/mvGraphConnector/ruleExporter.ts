import {Exporter} from "../rdfExport/exporter";

export class RuleExporter {
    private exporter: Exporter;
    private ruleCount = 1;

    constructor(filename: string) {
        this.exporter = new Exporter(filename);
        this.exporter.addLine('name|id|nameInGraph\n');
    }

    public addRule(name: string, id: string) {
        const ruleName = 'rule_' + this.ruleCount;
        this.exporter.addLine(ruleName + '|' + id + '|' + name + '\n');
        this.ruleCount++;
        return ruleName;
    }
}
