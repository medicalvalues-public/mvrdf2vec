import {Graph} from "../../graph/graph";

export class GetDisease {
    static exec(disease: any, ruleName: string, graph: Graph): void {
        // set type to currently examined with if there is no type yet
        const type = disease.type ? disease.type : 'currently_examined_with';
        const description = disease.disease.description;
        const icd10 = disease.disease.icd10;
        const snomed = disease.disease.snomed;
        const connectionType = disease.connectionType;
        if (icd10) {
            const parsedDisease = graph.diseases.getOrCreate(icd10, description, disease.disease.id, type);
            const triple = parsedDisease.exportDiseaseRule(ruleName, connectionType, graph);
            graph.addTriple(triple);
        } else {
            console.log('Disease can not be exported because of missing ICD10 code. Description: ' + description);
        }
    }
}
