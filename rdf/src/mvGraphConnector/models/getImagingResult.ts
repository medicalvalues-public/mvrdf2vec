import {Graph} from "../../graph/graph";
import {ImagingResult} from "../../graph/models/risk-factors/imagingResult";

export class GetImagingResult {
    static exec(imagingResult: any, ruleName: string, graph: Graph): void {
        const type = imagingResult.type;
        const description = imagingResult.imagingresult.description;
        const snomed = imagingResult.imagingresult.snomed;
        const connectionType = imagingResult.connectionType;
        const parsedImaging = new ImagingResult(description, snomed, imagingResult.imagingresult.id);
        graph.imagingResults.add(parsedImaging);
        const triple = parsedImaging.exportImagingResultRule(ruleName, type, connectionType, graph);
        graph.addTriple(triple);
    }
}
