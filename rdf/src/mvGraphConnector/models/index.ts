export { GetFinding } from "./getFinding";
export { GetDisease } from "./getDisease";
export { GetPathologicalReferenceRange } from "./getPathologicalReferenceRange";
export { GetImagingResult } from "./getImagingResult";
export { GetParameter } from "./GetParameter";
export { GetPatientBaseData } from "./getPatientBaseData";
