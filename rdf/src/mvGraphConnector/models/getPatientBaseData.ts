import {Graph} from "../../graph/graph";

export class GetPatientBaseData {
    static exec(patientBaseData: any, ruleName: string, graph: Graph): void {
        const maxAge = patientBaseData.patientbasedata.ageMax;
        const minAge = patientBaseData.patientbasedata.ageMin;
        const ethnicity = patientBaseData.patientbasedata.ethnicity;
        const gender = patientBaseData.patientbasedata.gender;
        const description = patientBaseData.patientbasedata.description;
        const id = patientBaseData.patientbasedata.id;
        const type = patientBaseData.type === 'currently_examined_with';
        const connectionType = patientBaseData.connectionType;
        const patientBaseDatum = graph.patientBaseData.getOrCreate(description, maxAge, minAge ,ethnicity, gender, id);
        const triple = patientBaseDatum.export(ruleName, type, connectionType, graph);
        graph.addTriple(triple);
    }
}
