import {Graph} from "../../graph/graph";
import {RuleExporter} from "../ruleExporter";

export class GetPathologicalReferenceRange {
    static multipleRules(referenceRanges: any, graph: Graph, ruleExporter: RuleExporter, ruleDescription: string, ruleId: string):
        any {
        const createdRules: string[] = [];
        const pathologicalReferenceRanges: { loinc: string }[] = []
        referenceRanges.forEach((referenceRange: any) => {
            if (referenceRange.referencerange.type === 'PathologicalReferenceRange') {
                const ageMin = referenceRange.referencerange.ageMin;
                const ageMax = referenceRange.referencerange.ageMax;
                const ageMinUnit = referenceRange.referencerange.ageMinUnit;
                const ageMaxUnit = referenceRange.referencerange.ageMaxUnit;
                const loinc = referenceRange.referencerange.parameter.loinc;
                const description = referenceRange.referencerange.description;
                const gender = referenceRange.referencerange.gender;
                const unit = referenceRange.referencerange.unit;
                const valueMin = referenceRange.referencerange.valueMin;
                const valueMax = referenceRange.referencerange.valueMax;
                pathologicalReferenceRanges.push({loinc});
                const pathologicalRange = graph.pathologicalRanges.getOrCreatePathologicalRange(description, ageMax, ageMaxUnit, ageMin,
                    ageMinUnit, unit, valueMax, valueMin, loinc, gender, referenceRange.referencerange.id);
                const newRuleName = ruleExporter.addRule(ruleDescription, ruleId);
                createdRules.push(newRuleName);
                const triple = pathologicalRange.exportPathologicalRangeRule(newRuleName, graph);
                graph.addTriple(triple);
            }
        });
        return {pathologicalReferenceRanges, createdRules};
    }

    public static singleRule(referenceRanges: any, graph: Graph, ruleName: string): any {
        const createdRules: string[] = [];
        const pathologicalReferenceRanges: { loinc: string }[] = []
        referenceRanges.forEach((referenceRange: any) => {
            if (referenceRange.referencerange.type === 'PathologicalReferenceRange') {
                const ageMin = referenceRange.referencerange.ageMin;
                const ageMax = referenceRange.referencerange.ageMax;
                const ageMinUnit = referenceRange.referencerange.ageMinUnit;
                const ageMaxUnit = referenceRange.referencerange.ageMaxUnit;
                const loinc = referenceRange.referencerange.parameter.loinc;
                const description = referenceRange.referencerange.description;
                const gender = referenceRange.referencerange.gender;
                const unit = referenceRange.referencerange.unit;
                const valueMin = referenceRange.referencerange.valueMin;
                const valueMax = referenceRange.referencerange.valueMax;
                pathologicalReferenceRanges.push({loinc});
                const pathologicalRange = graph.pathologicalRanges.getOrCreatePathologicalRange(description, ageMax, ageMaxUnit, ageMin,
                    ageMinUnit, unit, valueMax, valueMin, loinc, gender, referenceRange.referencerange.id);
                const triple = pathologicalRange.exportPathologicalRangeRule(ruleName, graph);
                graph.addTriple(triple);
            }
        });
        return {pathologicalReferenceRanges};
    }
}
