import {Graph} from "../../graph/graph";
import {Parameter} from "../../graph/models/risk-factors/parameter";

export class GetParameter {
    static exec(parameter: any, graph: Graph, ruleName: string): void {
        const loinc = parameter.parameter.loinc;
        const description = parameter.parameter.description;
        const type = parameter.type;
        const connectionType = parameter.connectionType;
        const parsedParam = new Parameter(description, loinc, parameter.parameter.id);
        graph.parameters.add(parsedParam);
        parsedParam.exportParameterRule(type, ruleName, connectionType, graph);
    }
}
