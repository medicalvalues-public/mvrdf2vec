import {Finding} from "../../graph/models/risk-factors/finding";
import {Graph} from "../../graph/graph";

export class GetFinding {
    static exec(finding: any, ruleName: string, graph: Graph): void {
        const snomed = finding.finding.snomed;
        const description = finding.finding.description;
        const type = finding.connectionType;
        const parsedFinding = new Finding(snomed, description, finding.finding.id);
        graph.findings.add(parsedFinding);
        const triple = parsedFinding.exportFindingRule(ruleName, finding.type, type, graph);
        graph.addTriple(triple);
    }
}
