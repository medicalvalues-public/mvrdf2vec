import {Request} from "../../../network/request";
import {allDiseases, pathDiseases} from "../../query";

export class Diseases {
    public static async getAll(): Promise<any[]> {
        const diseases = await Request.exec(allDiseases);
        return diseases.diseases.nodes;
    }

    public static async getPathDiseases(): Promise<any[]> {
        const queryResult = await Request.exec(pathDiseases);
        const allDiseases: any[] = [];
        for (const path of queryResult.pathdata.nodes) {
            const disease = path.diseasesByPathdataId.nodes[0];
            if (disease?.icd10) {
                if (allDiseases.find((double: any) => {
                    return double.icd10 === disease.icd10;
                })) {
                    console.error('More than one disease for icd code: ' + disease.icd10);
                } else {
                    allDiseases.push({
                        description: disease.description,
                        icd10: disease.icd10
                    });
                }
            }
        }
        return allDiseases;
    }

    public static async getPathDiseasesMap(): Promise<Map<string, string[]>> {
        const map = new Map();
        const queryResult = await Request.exec(pathDiseases);
        for (const path of queryResult.pathdata.nodes) {
            const disease = path.diseasesByPathdataId.nodes[0];
            if (disease?.icd10) {
                const currentDiseases = map.get(disease.icd10);
                if (currentDiseases) {
                    currentDiseases.push(disease.description)
                } else {
                    map.set(disease.icd10, [disease.description]);
                }
            }
        }
        return map;
    }
}