import {Request} from "../../../network/request";
import {allParameters} from "../../query";

export class Parameters {
    public static async getAll(): Promise<any[]> {
        const parameters = await Request.exec(allParameters);
        return parameters.parameters.nodes;
    }
}