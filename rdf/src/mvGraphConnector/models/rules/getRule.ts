import {
    GetImagingResult,
    GetDisease,
    GetParameter,
    GetPatientBaseData,
    GetFinding
} from "../index";
import {Graph} from "../../../graph/graph";
import {RuleExporter} from "../../ruleExporter";

export class GetRule {
    public static multipleRules(rule: any, weight: number, graph: Graph, ruleNames: string[]): void {
        rule.ruleDiseases.nodes.forEach((disease: any) => {
            if (disease.connectionType !== 'SIGNALS') {
                for (const ruleName of ruleNames) {
                    GetDisease.exec(disease, ruleName, graph);
                }
            }
        });
        rule.ruleFindings.nodes.forEach((finding: any) => {
            if (finding.connectionType !== 'SIGNALS') {
                for (const ruleName of ruleNames) {
                    GetFinding.exec(finding, ruleName, graph);
                }
            }
        });
        rule.ruleImagingresults.nodes.forEach((imagingResult: any) => {
            if (imagingResult.connectionType !== 'SIGNALS') {
                for (const ruleName of ruleNames) {
                    GetImagingResult.exec(imagingResult, ruleName, graph);
                }
            }
        });
        rule.ruleParameters.nodes.forEach((parameter: any) => {
            if (parameter.connectionType !== 'SIGNALS') {
                for (const ruleName of ruleNames) {
                    GetParameter.exec(parameter, graph, ruleName);
                }
            }
        });
        rule.rulePatientbasedata.nodes.forEach((patientBaseData: any) => {
            if (patientBaseData.connectionType !== 'SIGNALS') {
                for (const ruleName of ruleNames) {
                    GetPatientBaseData.exec(patientBaseData, ruleName, graph);
                }
            }
        });
    }

    public static singleRule(rule: any, graph: Graph, ruleExporter: RuleExporter, rules: any[]): string {
        const riskFactors: {type: string, id: string}[] = [];
        rule.ruleDiseases.nodes.forEach((disease: any) => {
            if (disease.connectionType !== 'SIGNALS') {
                riskFactors.push({
                    type: disease.connectionType,
                    id: disease.disease.id
                });
            }
        });
        rule.ruleFindings.nodes.forEach((finding: any) => {
            if (finding.connectionType !== 'SIGNALS') {
                riskFactors.push({
                    type: finding.connectionType,
                    id: finding.finding.id
                });
            }
        });
        rule.ruleImagingresults.nodes.forEach((imagingResult: any) => {
            if (imagingResult.connectionType !== 'SIGNALS') {
                riskFactors.push({
                    type: imagingResult.connectionType,
                    id: imagingResult.imagingresult.id
                });
            }
        });
        rule.ruleParameters.nodes.forEach((parameter: any) => {
            if (parameter.connectionType !== 'SIGNALS') {
                riskFactors.push({
                    type: parameter.connectionType,
                    id: parameter.parameter.id
                });
            }
        });
        rule.rulePatientbasedata.nodes.forEach((patientBaseData: any) => {
            if (patientBaseData.connectionType !== 'SIGNALS') {
                riskFactors.push({
                    type: patientBaseData.connectionType,
                    id: patientBaseData.patientbasedata.id
                });
            }
        });
        const foundRule = rules.find((rule) => {
            let found = true;
            for (const factor of rule.riskFactors) {
                if (!riskFactors.find((ruleFactor) => {
                    return ruleFactor.id === factor.id && ruleFactor.type === factor.type;
                })) {
                    found = false;
                }
            }
            return found;
        });
        let ruleName: string;
        if (foundRule) {
            ruleName = foundRule.name;
        } else {
            ruleName = ruleExporter.addRule(rule.description, rule.id);
            rules.push({
                name: ruleName,
                riskFactors
            });
            rule.ruleDiseases.nodes.forEach((disease: any) => {
                if (disease.connectionType !== 'SIGNALS' && disease.connectionType !== 'RESULTS_IN') {
                    GetDisease.exec(disease, ruleName, graph);
                }
            });
            rule.ruleFindings.nodes.forEach((finding: any) => {
                if (finding.connectionType !== 'SIGNALS' && finding.connectionType !== 'RESULTS_IN') {
                    GetFinding.exec(finding, ruleName, graph);
                }
            });
            rule.ruleImagingresults.nodes.forEach((imagingResult: any) => {
                if (imagingResult.connectionType !== 'SIGNALS') {
                    GetImagingResult.exec(imagingResult, ruleName, graph);
                }
            });
            rule.ruleParameters.nodes.forEach((parameter: any) => {
                if (parameter.connectionType !== 'SIGNALS') {
                    GetParameter.exec(parameter, graph, ruleName);
                }
            });
            rule.rulePatientbasedata.nodes.forEach((patientBaseData: any) => {
                if (patientBaseData.connectionType !== 'SIGNALS') {
                    GetPatientBaseData.exec(patientBaseData, ruleName, graph);
                }
            });
        }
       return ruleName;
    }
}
