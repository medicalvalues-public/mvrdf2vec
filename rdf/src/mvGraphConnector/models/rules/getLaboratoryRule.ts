import {GetParameter, GetPathologicalReferenceRange} from "../index";
import {Graph} from "../../../graph/graph";
import {RuleExporter} from "../../ruleExporter";

export class GetLaboratoryRule {
    public static multipleRules(rule: any, graph: Graph, ruleExporter: RuleExporter): string[] {
        const ruleNames: string[] = [];
        const {pathologicalReferenceRanges, createdRules}: any =
            GetPathologicalReferenceRange.multipleRules(rule.ruleReferenceranges.nodes, graph, ruleExporter, rule.description, rule.id);
        ruleNames.push(... createdRules);
        rule.ruleParameters.nodes.forEach((parameter: any) => {
            if (parameter.connectionType === 'SIGNALS' && !pathologicalReferenceRanges.find(
                (referenceRange: any) =>
                referenceRange.loinc === parameter.parameter.loinc)) {
                const newRuleName = ruleExporter.addRule(rule.description, rule.id);
                ruleNames.push(newRuleName);
                GetParameter.exec(parameter, graph, newRuleName);
            }
        });
        return ruleNames;
    }

    public static singleRule(rule: any, graph: Graph, ruleName: string): void {
        const {pathologicalReferenceRanges}: any =
            GetPathologicalReferenceRange.singleRule(rule.ruleReferenceranges.nodes, graph, ruleName);
        rule.ruleParameters.nodes.forEach((parameter: any) => {
            if (parameter.connectionType === 'SIGNALS' && !pathologicalReferenceRanges.find(
                (referenceRange: any) =>
                    referenceRange.loinc === parameter.parameter.loinc)) {
                GetParameter.exec(parameter, graph, ruleName);
            }
        });
    }
}
