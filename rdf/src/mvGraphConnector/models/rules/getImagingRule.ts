import { GetImagingResult } from "../index";
import {Graph} from "../../../graph/graph";
import {RuleExporter} from "../../ruleExporter";

export class GetImagingRule {
    public static multipleRules(rule: any, graph: Graph, ruleExporter: RuleExporter): string[] {
        const createdRules: string[] = [];
        rule.ruleImagingresults.nodes.forEach((imagingResult: any) => {
            if (imagingResult.connectionType === 'SIGNALS') {
                const newRuleName = ruleExporter.addRule(rule.description, rule.id);
                GetImagingResult.exec(imagingResult, newRuleName, graph);
                createdRules.push(newRuleName);
            }
        });
        return createdRules;
    }

    public static singleRule(rule: any, graph: Graph, ruleName: string): void {
        rule.ruleImagingresults.nodes.forEach((imagingResult: any) => {
            if (imagingResult.connectionType === 'SIGNALS') {
                GetImagingResult.exec(imagingResult, ruleName, graph);
            }
        });
    }
}
