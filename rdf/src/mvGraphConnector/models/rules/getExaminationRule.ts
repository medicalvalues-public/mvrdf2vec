import {GetDisease, GetFinding, GetPatientBaseData} from "../index";
import {Graph} from "../../../graph/graph";
import {RuleExporter} from "../../ruleExporter";

export class GetExaminationRule {
    public static multipleRules(rule: any, graph: Graph, ruleExporter: RuleExporter): string[] {
        const resultingRules: string[] = [];
        rule.ruleDiseases.nodes.forEach((disease: any) => {
            if (disease.connectionType === 'SIGNALS') {
                const newRuleName = ruleExporter.addRule(rule.description, rule.id);
                resultingRules.push(newRuleName);
                GetDisease.exec(disease, newRuleName, graph);
            }
        });
        rule.ruleFindings.nodes.forEach((finding: any) => {
            if (finding.connectionType === 'SIGNALS') {
                const newRuleName = ruleExporter.addRule(rule.description, rule.id);
                resultingRules.push(newRuleName);
                GetFinding.exec(finding, newRuleName, graph);
            }
        });
        rule.rulePatientbasedata.nodes.forEach((patientBaseData: any) => {
            if (patientBaseData.connectionType === 'SIGNALS') {
                const newRuleName = ruleExporter.addRule(rule.description, rule.id);
                resultingRules.push(newRuleName);
                GetPatientBaseData.exec(patientBaseData, newRuleName, graph);
            }
        });
        return resultingRules;
    }

    public static singleRule(rule: any, graph: Graph, ruleName: string): void {
        rule.ruleDiseases.nodes.forEach((disease: any) => {
            if (disease.connectionType === 'SIGNALS') {
                GetDisease.exec(disease, ruleName, graph);
            }
        });
        rule.ruleFindings.nodes.forEach((finding: any) => {
            if (finding.connectionType === 'SIGNALS') {
                GetFinding.exec(finding, ruleName, graph);
            }
        });
        rule.rulePatientbasedata.nodes.forEach((patientBaseData: any) => {
            if (patientBaseData.connectionType === 'SIGNALS') {
                GetPatientBaseData.exec(patientBaseData, ruleName, graph);
            }
        });
    }
}
