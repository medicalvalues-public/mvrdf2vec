export const pathQuery = `
{
                nodes
                {
                    description
                    id
                    severity
                    status
                    frequency
                    pathdataRulesByPathdataId
                    {
                        nodes
                        {
                            weight
                            connectionType
                            rule
                            {
                                id
                                description
                                type
                                ruleDiseases
                                {
                                    nodes
                                    {
                                        connectionType
                                        type
                                            disease
                                        {
                                            id
                                            description
                                            icd10
                                            snomed
                                        }
                                    }
                                }
                                ruleExternalsystems
                                {
                                    nodes
                                    {
                                        connectionType
                                        externalsystem
                                        {
                                            description
                                            id
                                            script
                                            diseaseExternalsystems {
                                                nodes {
                                                    connectionType
                                                    disease {
                                                        description
                                                        icd10
                                                        snomed
                                                    }
                                                }
                                            }
                                            findingExternalsystems {
                                                nodes {
                                                    connectionType
                                                    finding {
                                                        description
                                                        snomed
                                                    }
                                                }
                                            }
                                            imagingresultExternalsystems {
                                                nodes {
                                                    connectionType
                                                    imagingresult {
                                                        id
                                                        description
                                                        snomed
                                                    }
                                                }
                                            }
                                            parameterExternalsystems {
                                                nodes {
                                                    connectionType
                                                    parameter {
                                                        description
                                                        abbreviation
                                                        loinc
                                                        defaultUnit
                                                    }
                                                }
                                            }
                                            patientbasedataExternalsystems {
                                                nodes {
                                                    connectionType
                                                    patientbasedata {
                                                        id
                                                        description
                                                        ageMax
                                                        ageMaxUnit
                                                        ageMin
                                                        ageMinUnit
                                                        ethnicity
                                                        gender
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                ruleFindings
                                {
                                    nodes
                                    {
                                        connectionType
                                        type
                                            finding
                                        {
                                            id
                                            description
                                            snomed
                                        }
                                    }
                                }
                                ruleImagingresults
                                {
                                        nodes
                                        {
                                            connectionType
                                            type
                                                imagingresult
                                            {
                                                id
                                                description
                                                snomed
                                            }
                                        }
                                }
                                ruleParameters
                                {
                                    nodes
                                    {
                                        connectionType
                                        type
                                        parameter
                                        {
                                            id
                                            abbreviation
                                            calculated
                                            cost
                                            description
                                            loinc
                                        }
                                        type
                                    }
                                }
                                core
                                description
                                ruleAlternatives
                                {
                                    nodes
                                    {
                                        connectionType
                                        alternative
                                        {
                                            description
                                            ruleAlternatives
                                            {
                                                nodes
                                                {
                                                    rule
                                                    {
                                                        id
                                                        description
                                                    }
                                                }    
                                            }
                                        }
                                    }
                                }
                                rulePatientbasedata
                                {
                                    nodes
                                    {
                                        connectionType
                                        type
                                            patientbasedata
                                        {
                                            id
                                            ageMax
                                            ageMin
                                            description
                                            ethnicity
                                            gender
                                        }
                                    }
                                }
                                ruleReferenceranges
                                {
                                    nodes
                                    {
                                        connectionType
                                        referencerange
                                        {
                                            id
                                            ageMax
                                            ageMaxUnit
                                            ageMin
                                            ageMinUnit
                                            description
                                            gender
                                            source
                                            {
                                                rating
                                                url
                                                description
                                            }
                                            type
                                            unit
                                            valueMax
                                            valueMin
                                            parameter {
                                                description
                                                loinc
                                            }
                                        }
                                    }
                                }
                                ruleSources
                                {
                                    nodes
                                    {
                                        source
                                        {
                                            description
                                            rating
                                            url
                                        }
                                    }
                                }
                            }
                        }
                    }
                    diseasesByPathdataId {
                        nodes {
                            description
                            icd10
                            snomed
                        }
                    }
                }
            }
`;

export const allDiseases = `query MyQuery {
  diseases {
    nodes {
      description
      icd10
    }
  }
}
`

export const allParameters = `query MyQuery {
  parameters {
    nodes {
      description
      loinc
    }
  }
}
`;

export const pathDiseases = `query MyQuery {
  pathdata {
    nodes {
      diseasesByPathdataId {
        nodes {
          icd10
          description
        }
      }
      description
    }
  }
}
`;