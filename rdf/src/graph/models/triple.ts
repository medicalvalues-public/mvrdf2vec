import {ReplaceWrongCharacters} from "../../services/replaceWrongCharacters";

export class Triple {
    public from: string;
    public to: string;
    public link: string

    constructor(from: string, to: string, link: string) {
        this.from = ReplaceWrongCharacters.exec(from);
        this.to = ReplaceWrongCharacters.exec(to);
        this.link = ReplaceWrongCharacters.exec(link);
    }
}
