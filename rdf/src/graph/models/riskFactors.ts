import {ReplaceWrongCharacters} from "../../services/replaceWrongCharacters";
import {Exporter} from "../../rdfExport/exporter";

export class RiskFactors {
    riskFactors: Map<string, {description: string, type: string}> = new Map<string, {description: string; type: string}>();

    public add(description: string, type: string, id: string): void {
        const parsedDescription = ReplaceWrongCharacters.exec(description);
        if (!this.riskFactors.has(id)) {
            this.riskFactors.set(id, {
                description: parsedDescription,
                type
            });
        }
    }

    public async export(filename: string): Promise<void> {
        const riskFactors = new Exporter(filename);
        riskFactors.addLine('name|type\n');
        for (const factor of this.riskFactors) {
            riskFactors.addLine(ReplaceWrongCharacters.exec(factor[1].description) + '|' +
                ReplaceWrongCharacters.exec(factor[1].type) + '\n');
        }
    }

}