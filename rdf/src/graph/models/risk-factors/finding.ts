import {Triple} from "../triple";
import {Graph} from "../../graph";

export class Finding {
    public snomed: string;
    public description: string;
    public id: string

    constructor(snomed: string, description: string, id: string) {
        this.snomed = snomed;
        this.description = description;
        this.id = id;
    }

    public exportFindingRule(ruleName: string, type: string, connectionType: string, graph: Graph): Triple {
        const description = type + '_' + this.description;
        graph.existingRiskRelations.push({
            riskFactor: description,
            ruleName,
            type: connectionType
        });
        graph.riskFactors.add(description, 'finding', this.id);
        if (connectionType === 'SIGNALS'){
            return new Triple(description, ruleName, 'signals');
        } else if (connectionType === 'RESTRICTS') {
            return new Triple(ruleName, description, 'restricts');
        } else if (connectionType === 'REQUIRES') {
            return new Triple(ruleName, description, 'required_by');
        }
        console.log(connectionType);
        throw new Error();
    }
}
