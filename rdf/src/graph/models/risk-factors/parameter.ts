import {Triple} from "../triple";
import {Graph} from "../../graph";

export class Parameter {
    public loinc: string;
    public description: string;
    public id: string

    constructor(description: string, loinc: string, id: string) {
        this.loinc = loinc;
        this.description = description;
        this.id = id;
    }

    public exportParameterRule(evaluation: string, ruleName: string, type: string, graph: Graph): void {
        const description = this.description + '_' + evaluation;
        if (graph.exportConfig.riskFactorInPatientDataNeeded) {
            if (graph.statsManager.getParameterCount(this.loinc) >= graph.exportConfig.minFactorCount) {
                graph.riskFactors.add(description, 'parameter', this.id);
                graph.existingRiskRelations.push({
                    riskFactor: description,
                    ruleName,
                    type
                });
            }
        } else {
            graph.riskFactors.add(description, 'parameter', this.id);
            graph.existingRiskRelations.push({
                riskFactor: description,
                ruleName,
                type
            });
        }
        if (type === 'SIGNALS') {
            graph.addTriple(new Triple(description, ruleName, 'signals'));

        } else if (type === 'REQUIRES') {
            graph.addTriple(new Triple(ruleName, description, 'requires'));

        } else if (type === 'RESTRICTS') {
            graph.addTriple(new Triple(ruleName, description, 'restricted_by'));
        } else {
            throw new Error('Wrong input for parameter export.');
        }
    }
}
