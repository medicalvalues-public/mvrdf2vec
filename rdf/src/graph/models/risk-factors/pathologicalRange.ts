import {UnitConverter} from "../../../services/unitCoverter";
import {Patient} from "../../../patient/patient";
import {Triple} from "../triple";
import {Graph} from "../../graph";

export class PathologicalRange {
    description: string;
    ageMax: number = -1;
    ageMaxUnit: string= 'unknown';
    ageMin: number = -1;
    ageMinUnit: string= 'unknown';
    unit: string = 'unknown';
    valueMax: number = -1;
    valueMin: number = -1;
    loinc: string = 'unknown';
    gender: string = 'unknown';
    id: string

    constructor(description: string, ageMax: number, ageMaxUnit: string, ageMin: number,
                ageMinUnit: string,
                unit: string, valueMax: number, valueMin: number, loinc: string, gender: string, id: string) {
        this.description = description;
        this.ageMax = ageMax;
        this.ageMaxUnit = ageMaxUnit;
        this.ageMin = ageMin;
        this.ageMinUnit = ageMinUnit;
        this.unit = unit;
        this.valueMin = valueMin;
        this.valueMax = valueMax;
        this.loinc =loinc;
        this.gender = gender;
        this.id = id;
    }

    public exportPathologicalRangeRule(ruleName: string, graph: Graph): Triple {
        if (graph.exportConfig.riskFactorInPatientDataNeeded) {
            if (graph.statsManager.getParameterCount(this.loinc) >= graph.exportConfig.minFactorCount) {
                graph.riskFactors.add(this.description, 'pathologicalReferenceRange', this.id);
                graph.existingRiskRelations.push({
                    riskFactor: this.description,
                    ruleName: ruleName,
                    type: 'is_in'
                });
            }
        } else {
            graph.riskFactors.add(this.description, 'pathologicalReferenceRange', this.id);
            graph.existingRiskRelations.push({
                riskFactor: this.description,
                ruleName: ruleName,
                type: 'is_in'
            });
        }
        return new Triple(this.description, ruleName, 'signals');
    }

    isInRange(paramValue: string, unit: string, patient: Patient): boolean {
            if (this.fitToPatient(patient)) {
                if (this.unit === 'YES/NO') {
                    console.info('Pathological Reference Range with YES/NO not implemented yet.');
                } else {
                    const value = parseFloat(paramValue);
                    const convertedValue = UnitConverter.convertParameterValue(value, unit, this.unit);
                    if (this.valueMin) {
                        if (this.valueMax) {
                            if (convertedValue >= this.valueMin && convertedValue <= this.valueMax) {
                                return  true;
                            }
                        } else {
                            if (convertedValue >= this.valueMin) {
                                return true;
                            }
                        }
                    } else if (this.valueMax) {
                        if (convertedValue <= this.valueMax) {
                            return true;
                        }
                    }
                }
            }
        return false;
    }

    private fitToPatient(patient: Patient): boolean {
        let min = null;
        let max = null;
        if (this.ageMin === null && this.ageMax === null && this.gender === null) {
            return true;
        }
        if (this.ageMin && this.ageMinUnit) {
            min = UnitConverter.convertAgeToHours(this.ageMin, this.ageMinUnit);
        }
        if (this.ageMax && this.ageMaxUnit) {
            max = UnitConverter.convertAgeToHours(this.ageMax, this.ageMaxUnit);
        }
        if (this.gender) {
            if (patient.gender === this.gender) {
                return this.fitToAge(min, max, patient);
            }
        } else if (min || max) {
            return this.fitToAge(min, max, patient);
        }
        return false;
    }

    private fitToAge(age_min: number | null, age_max: number | null, patient: Patient): boolean {
        if (age_min && age_min !== -1) {
            if (age_max && age_max !== -1) {
                if (patient.age >= age_min && patient.age <= age_max) {
                    return true;
                }
            } else {
                if (patient.age >= age_min) {
                    return true;
                }
            }
        } else if (age_max && age_max !== -1) {
            if (patient.age <= age_max) {
                return true;
            }
        }
        return false;
    }
}
