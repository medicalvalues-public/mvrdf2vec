import {Triple} from "../triple";
import {Graph} from "../../graph";
import {IcdFilter} from "../../../services/icdFilter";

export class Disease {
    public icd10: string
    public description: string;
    public type: string;
    public id: string;

    constructor(icd10: string, description: string, id: string, type: string) {
        this.icd10 = IcdFilter.replacePoints(icd10);
        this.description = description;
        this.type = type;
        this.id = id;
    }

    public exportDiseaseRule(ruleName: string, type: string, graph: Graph): Triple {
        let description = this.description+ '_' + this.type;
        if (type !== 'DEFINES') {
            graph.riskFactors.add(description, 'disease', this.id);
            graph.existingRiskRelations.push({
                riskFactor: description,
                ruleName,
                type
            })
        }
        if (type === 'SIGNALS') {
            return new Triple(description, ruleName, 'signals');
        } else if (type === 'REQUIRES') {
            return new Triple(ruleName, description, 'requires');
        } else if (type === 'RESTRICTS') {
            return new Triple(ruleName, description, 'restricted_by');
        } else if (type === 'DEFINES') {
            return new Triple(ruleName, description, 'increases_risk');
        }
        throw new Error();
    }
}
