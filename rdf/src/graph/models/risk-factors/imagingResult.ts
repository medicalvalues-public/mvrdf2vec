import {Triple} from "../triple";
import {Graph} from "../../graph";
export class ImagingResult {
    public snomed: string;
    public description: string;
    public id: string

    constructor(description: string, snomed: string, id: string) {
        this.description = description;
        this.snomed = snomed;
        this.id = id;
    }

    public exportImagingResultRule(ruleName: string, evaluation: string, type: string, graph: Graph): Triple {
        const description = this.description + '_' + evaluation;
        graph.riskFactors.add(description, 'imagingResult', this.id);
        graph.existingRiskRelations.push({
            riskFactor: description,
            ruleName,
            type
        });
        if (type === 'SIGNALS') {
            return new Triple(description, ruleName, 'signals');
        } else if (type === 'REQUIRES') {
            return new Triple(description, ruleName, 'requires');
        } else if (type === 'RESTRICTS') {
            return new Triple(ruleName, description, 'restricted_by');
        }
        throw new Error();
    }
 }
