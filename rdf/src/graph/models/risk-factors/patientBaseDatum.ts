import {Patient} from "../../../patient/patient";
import {Triple} from "../triple";
import {Graph} from "../../graph";

export class PatientBaseDatum {
    public description: string;
    public ageMin: number | null;
    public ageMax: number | null;
    public id: string;
    private readonly ethnicity: string | null;
    private readonly gender: string | null;

    constructor(description: string, ageMax: number | null, ageMin: number | null, ethnicity: string | null,
                gender: string | null, id: string) {
        this.description = description;
        this.ageMax = ageMax;
        this.ageMin = ageMin;
        this.ethnicity = ethnicity;
        this.gender = gender;
        this.id = id;
        this.checkConsistency();
    }

    public export(rule: string, type: boolean, connectionType: string, graph: Graph): Triple {
        const description = (type ? 'currently_examined_with' : 'currently_not_examined_with')
            + '_' + this.description;
        graph.riskFactors.add(description, 'patientBaseData', this.id);
        graph.existingRiskRelations.push({
            riskFactor: description,
            ruleName: rule,
            type: 'signals'
        });
        if (connectionType === 'SIGNALS') {
            return new Triple(description, rule, 'signals');
        } else if (connectionType === 'REQUIRES') {
            return new Triple(rule, description, 'requires');
        } else if (connectionType === 'RESTRICTS') {
            return new Triple(rule, description, 'restricted_by');
        }
    throw new Error();
    }

    public evaluate(patient: Patient, mustBeInPatientBaseData: boolean): boolean {
        const patientInPatientBaseData = this.fitToPatientBaseData(patient);
        if (mustBeInPatientBaseData !== null) {
            if (mustBeInPatientBaseData && patientInPatientBaseData) {
                return true;
            } else if (!mustBeInPatientBaseData && !patientInPatientBaseData) {
                return true;
            }
        } else {
            return patientInPatientBaseData;
        }
        return false;
    }

    private fitToPatientBaseData(patient: Patient): boolean {
        if (this.ethnicity) {
            if (patient.ethnicity && (this.ethnicity === patient.ethnicity)) {
                if (this.gender === patient.gender || this.gender === null) {
                    if (this.fitToAge(patient)) {
                        return true;
                    }
                }
            }
        } else if (this.gender) {
            if (this.gender === patient.gender) {
                if (this.fitToAge(patient)) {
                    return true;
                }
            }
        } else if (this.ageMin || this.ageMax) {
            if (this.fitToAge(patient)) {
                return true;
            }
        }
        return false;
    }

    private fitToAge(patient: Patient): boolean {
        if (!this.ageMin && !this.ageMax) {
            return true;
        } else if (this.ageMin && !this.ageMax) {
            if (patient.age >= (this.ageMin * 365 * 24)) {
                return true;
            }
        } else if (this.ageMax && !this.ageMin) {
            if (patient.age <= (this.ageMax * 365 * 24)) {
                return true;
            }
        } else if (this.ageMin && this.ageMax) {
            if (patient.age >= (this.ageMin * 365 * 24) && patient.age <= (this.ageMax * 365 * 24)) {
                return true;
            }
        }
        return false;
    }

    private checkConsistency(): void {
        if (!this.gender && !this.ageMin && !this.ageMax && !this.ethnicity) {
            console.error('PatientBaseDatum ' + this.description + ' has no properties');
        }
    }
}
