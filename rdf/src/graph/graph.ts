import {Findings} from "./graphProperties/findings";
import {ImagingResults} from "./graphProperties/imagingResults";
import {Parameters} from "./graphProperties/parameters";
import {PatientBaseData} from "./graphProperties/patientBaseData";
import {Triple} from "./models/triple";
import {Diseases} from "./graphProperties/diseases";
import {PathologicalRanges} from "./graphProperties/pathologicalRanges";
import {ExportInterface} from "../rdfExport/export.interface";
import {RiskFactors} from "./models/riskFactors";
import {Exporter} from "../rdfExport/exporter";
import {ReplaceWrongCharacters} from "../services/replaceWrongCharacters";
import {RdfModel} from "./rdfModel";
import {StatsManager} from "../patient/stats/statsManager";

export class Graph {
    // settings
    rdfModel: RdfModel;

    public findings: Findings = new Findings();
    public imagingResults: ImagingResults = new ImagingResults();
    public parameters: Parameters = new Parameters();
    public diseases: Diseases = new Diseases();
    public patientBaseData: PatientBaseData = new PatientBaseData();
    public pathologicalRanges: PathologicalRanges = new PathologicalRanges();
    public riskFactors: RiskFactors = new RiskFactors();
    public existingRiskRelations: {riskFactor: string, ruleName: string, type: string}[] = [];
    public triples: Triple[] = [];
    public statsManager: StatsManager;
    public exportConfig: {riskFactorInPatientDataNeeded: boolean,
        minFactorCount: number};

    constructor(model: RdfModel, statsManager: StatsManager, exportConfig: {riskFactorInPatientDataNeeded: boolean,
    minFactorCount: number}) {
        this.rdfModel = model;
        this.statsManager = statsManager;
        this.exportConfig = exportConfig;
    }

    public async export(exporter: ExportInterface): Promise<void> {
        for (const triple of this.triples) {
            await exporter.export(triple);
        }
    }

    public addTriple(triple: Triple): void {
        this.triples.push(triple);
    }

    public async exportRiskRelations(filename: string): Promise<void> {
        const relationsExporter = new Exporter(filename);
        const firstLine = 'risk_factor|rule|type\n';
        relationsExporter.addLine(firstLine);
        for (const relation of this.existingRiskRelations) {
            relationsExporter.addLine(ReplaceWrongCharacters.exec(relation.riskFactor) + '|' +
                ReplaceWrongCharacters.exec(relation.ruleName) + '|' + relation.type + '\n');
        }
    }
}
