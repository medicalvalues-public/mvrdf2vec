import {PatientBaseDatum} from "../models/risk-factors/patientBaseDatum";
import {Patient} from "../../patient/patient";
import {Triple} from "../models/triple";

export class PatientBaseData {
    public patientBaseData: Map<string, PatientBaseDatum> = new Map<string, PatientBaseDatum>();

    public getOrCreate(description: string, maxAge: number, minAge: number, ethnicity: string, gender: string, id: string):
        PatientBaseDatum {
        let patientBaseDatum = this.patientBaseData.get(description);
        if (patientBaseDatum) {
            return patientBaseDatum;
        }
        patientBaseDatum = new PatientBaseDatum(description, maxAge, minAge ,ethnicity, gender, id);
        this.patientBaseData.set(description, patientBaseDatum);
        return patientBaseDatum;
    }

    public patientInPatientBaseData(patient: Patient): Triple[] {
        const triples = [];
        for (const baseDatum of this.patientBaseData.values()) {
            if (baseDatum.evaluate(patient, true)) {
                triples.push(new Triple(patient.description, 'currently_examined_with' + '_' + baseDatum.description,
                    'has'));
            } else if (baseDatum.evaluate(patient, false)) {
                triples.push(new Triple(patient.description, 'currently_not_examined_with' + '_' + baseDatum.description,
                    'has'));
            }
        }
        return triples;
    }
}
