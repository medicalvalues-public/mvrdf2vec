import {Parameter} from "../models/risk-factors/parameter";

export class Parameters {
    public parameters: Parameter[] = [];

    public add(parameter: Parameter): void {
        this.parameters.push(parameter);
    }

    public getDescription(loinc: string): string | null {
        const found = this.parameters.find((parameter) => {
            return parameter.loinc === loinc;
        });
        if (found) {
            return found.description;
        }
        return null;
    }
}
