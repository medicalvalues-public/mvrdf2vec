import {Finding} from "../models/risk-factors/finding";

export class Findings {
    public findings: Finding[] = [];

    public add(finding: Finding) {
        this.findings.push(finding);
    }

    public getDescription(snomed: string): string | null {
        const finding = this.findings.find((finding) => {
            return finding.snomed === snomed;
        });
        if (finding) {
            return finding.description;
        }
        return null;
    }
}
