import {Disease} from "../models/risk-factors/disease";
import {IcdFilter} from "../../services/icdFilter";

export class Diseases {
    public diseases: Map<string, Disease[]> = new Map<string, Disease[]>();

    public getOrCreate(icd10: string, description: string, id: string, type: string): Disease {
        icd10 = IcdFilter.replacePoints(icd10);
        let diseases = this.diseases.get(icd10);
        if (diseases) {
            const found = diseases.find((disease) => {
                return disease.description === description && disease.type === type && disease.icd10 === icd10 &&
                    disease.id === id;
            });
            if (found) {
                return found;
            } else {
                const createdDisease = new Disease(icd10, description, id, type);
                diseases.push(createdDisease);
                return createdDisease
            }
        } else {
            const createdDisease = new Disease(icd10, description, id, type);
            this.diseases.set(icd10, [createdDisease]);
            return createdDisease;
        }
    }
}
