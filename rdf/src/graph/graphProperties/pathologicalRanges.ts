import {PathologicalRange} from "../models/risk-factors/pathologicalRange";
import {Triple} from "../models/triple";
import {Patient} from "../../patient/patient";

export class PathologicalRanges {
    pathologicalRanges: Map<string, PathologicalRange> = new Map<string, PathologicalRange>();

    public getOrCreatePathologicalRange(description: string, ageMax: number, ageMaxUnit: string, ageMin: number,
                                        ageMinUnit: string, unit: string, valueMax: number, valueMin: number, loinc: string,
                                        gender: string, id: string): PathologicalRange {
        let range = this.pathologicalRanges.get(description);
        if (range) {
            return range;
        }
        range = new PathologicalRange(description, ageMax, ageMaxUnit, ageMin,
            ageMinUnit, unit, valueMax, valueMin, loinc, gender, id);
        this.pathologicalRanges.set(description, range);
        return range;
    }

    public parameterInPathologicalRanges(loinc: string, value: number, unit: string, patient: Patient): Triple[] {
        const triples = [];
        for (const pair of this.pathologicalRanges) {
            const range = pair[1];
            if (range.loinc === loinc) {
                if (range.isInRange('' + value, unit, patient)) {
                    const triple = new Triple(patient.id, range.description, 'has');
                    triples.push(triple);
                }
            }
        }
        return triples;
    }
}
