import {ImagingResult} from "../models/risk-factors/imagingResult";

export class ImagingResults {
    public imagingResults: ImagingResult[] = [];

    public add(imagingResult: ImagingResult): void {
        this.imagingResults.push(imagingResult);
    }

    public getDescription(snomed: string): string | null {
        const imagingResult = this.imagingResults.find((result) => {
            return result.snomed === snomed;
        });
        if (imagingResult) {
            return imagingResult.description;
        }
        return null;
    }
}
