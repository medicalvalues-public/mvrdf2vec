export class IcdFilter {
    public static isValid(icd10: string): boolean {
        return (!(icd10.includes('-') || icd10.includes('+') || icd10.includes('*') ||
            icd10.includes('!') || icd10 === '0'));
    }

    public static replacePoints(icd10: string): string {
        return icd10.replace('.', '');
    }
}