const ucum = require('@lhncbc/ucum-lhc');

export class UnitConverter {
    public static convertParameterValue(value: number, fromUnit: string, toUnit: string) {
        const filteredFromUnit = UnitConverter.filterParameterUnits(fromUnit);
        const ucumUtils = ucum.UcumLhcUtils.getInstance();
        if (fromUnit !== toUnit) {
            const returnObj = ucumUtils.convertUnitTo(filteredFromUnit, value, toUnit);
            if (returnObj['status'] === 'succeeded') {
                return returnObj['toVal'];
            } else {
                const message = returnObj['status'] + " " + returnObj['msg'];
                console.error('Unit conversion error. From: ' + fromUnit + ', to: ' + toUnit);
                return false;
            }
        } else {
            return value;
        }
    }

    /**
     * Converts an age in any unit to age in hours
     *
     * @param age value
     * @param unit of age value
     */
    public static convertAgeToHours(age: number, unit: string): number {
        switch (unit) {
            case 'hours':
                return age;
            case 'days':
                return age * 24;
            case 'weeks':
                return age * 7 * 24;
            case 'months':
                return age * 30 * 24;
            case 'years':
                return age * 30 * 12 * 24;
            default:
                return -1;
        }
    }

    public static convertIsoStringToHours(isoString: string): number {
        const date = new Date(isoString);
        const now = new Date();
        return (now.getTime() - date.getTime()) / (1000 * 60 * 60);
    }

    public static convertAgeUnitToHours(age: number | string, ageUnit: string): number {
        let convertedAge = -1;
        if (typeof age === 'string' && ageUnit && ageUnit.toLowerCase() === 'birthdate') {
            convertedAge = this.convertIsoStringToHours(age);
        } else if (typeof age === 'string') {
            age = parseInt(age);
            convertedAge = this.convertAgeToHours(age, ageUnit);
        } else {
            convertedAge = this.convertAgeToHours(age, ageUnit);
        }
        return convertedAge;
    }

    public static filterParameterUnits(unit: string): string {
        let filteredUnit = unit;
        switch (unit) {
            case 'G/l':
                filteredUnit = '10^9/l';
                break;
            case 'T/l':
                filteredUnit = '10^12/l';
                break;
            case 'Index':
                filteredUnit = '{index}';
                break;
            case 'sec.':
                filteredUnit = 's';
                break;
            case 'sec':
                filteredUnit = 's';
                break;
            case 'mU/l':
                filteredUnit = 'm[iU]/L'
                break;
            case 'IU':
                filteredUnit = '[IU]';
                break;
            case 'IU/L':
                filteredUnit = '[IU]/L';
                break;
            case 'mEq/L':
                filteredUnit = 'meq/L'
                break;
            case 'uIU/mL':
                filteredUnit = 'u[IU]/mL'
        }
        return filteredUnit;
    }
}
