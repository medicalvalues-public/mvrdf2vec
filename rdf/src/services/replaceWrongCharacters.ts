export class ReplaceWrongCharacters {
    public static exec(text: string): string {
        let result = text;
        // spaces
        result = result.replace(/\s/g, '_');
        // lower than
        result = result.replace(/</g, 'lower_than');
        // greater than
        result = result.replace(/>/g, 'greater_than');
        result = result.replace(/\u2265/g, 'greater_equal_than');
        result = result.replace(/\u2264/g, 'less_equal_than');
        result = result.replace(/\u03B2/g, 'beta');
        result = result.replace(/\u03B1/g, 'alpha');
        result = result.replace(/\u025B/g, 'in');
        result = result.replace(/\u03b4/g, 'delta');
        result = result.replace(/ä/g, 'ae');
        result = result.replace(/Ä/g, 'Ae');
        result = result.replace(/ö/g, 'oe');
        result = result.replace(/Ö/g, 'Oe');
        result = result.replace(/ü/g, 'ue');
        result = result.replace(/Ü/g, 'Ue');
        result = result.replace(/ß/g, 'ss');
        result = result.replace(/\u03B5/g, 'epsilon');
        return result;
    }
}