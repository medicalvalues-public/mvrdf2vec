import {Service} from "typedi";

@Service()
export class Logger {
    public log(text: string): void {
        console.log(text);
    }

    public error(text: string): void {
        console.error(text);
    }
}