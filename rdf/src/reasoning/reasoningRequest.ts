import config from './../config';
const axios = require('axios');

export class ReasoningRequest {
    public static async exec(path: string, body: any) {
        let result;
        await axios
            .post(config.reasoningUrl + path, body)
            .then((res: any) => {
                result =  res.data;
            })
            .catch((error: any) => {
                console.error(error)
            })
        return result;
    }
}