import {Exporter} from "./exporter";
import {Triple} from "../graph/models/triple";
import {ExportInterface} from "./export.interface";

export class NTriples extends Exporter implements ExportInterface{

    constructor(filename: string) {
        super(filename);
    }

    public async export(triple: Triple): Promise<void> {
        const line = NTriples.buildNode(triple.from) + ' ' + NTriples.buildLink(triple.link) + ' ' +
            NTriples.buildNode(triple.to) + ' .\n';
        this.addLine(line);
    }

    private static buildNode(description: string): string {
        return '<' + description + '>';
    }

    private static buildLink(description: string): string {
        return  '<' + description + '>';
    }
}
