export class Exporter {
    private readonly filename: string;
    private fs = require('fs');
    private path = require('path');


    constructor(filename: string) {
        this.filename = this.path.resolve(__dirname, '../' + filename);
        if (this.fs.existsSync(this.filename)) {
            this.fs.unlinkSync(this.filename);
        }
    }

    public addLine(line: string) {
        this.fs.appendFileSync(this.filename, line, (err: any) => {
            if (err) {
                console.error(err)
                return
            }
        });
    }
}
