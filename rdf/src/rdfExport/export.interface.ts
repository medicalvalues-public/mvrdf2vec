import {Triple} from "../graph/models/triple";

export interface ExportInterface {
    export(triple: Triple): Promise<void>;
}
