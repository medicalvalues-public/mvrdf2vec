import {Diseases} from "../../../mvGraphConnector/models/graphProperties/diseases";
import {Parameters} from "../../../mvGraphConnector/models/graphProperties/parameters";
import {Patient} from "../../patient";
import {UnitConverter} from "../../../services/unitCoverter";
import {AllAdmissionsForDisease} from "./db/allAdmissionsForDisease";
import {DiseasesForAdmission} from "./db/diseasesForAdmission";
import {PatientExporter} from "../../patientExporter";
import {AllPatientsForDisease} from "./db/allPatientsForDisease";
import {ExportPatient} from "../exportPatient";
import config from "../../../config";
import {IcdFilter} from "../../../services/icdFilter";

export class ExtractPatients extends ExportPatient {
    diseases: { description: string, code: string }[] = [];
    parameters: Promise<any[]>;
    matchedPatients = 0;
    diseaseLoaded: Promise<any>;
    diseasesMap: Map<string, string[]> = new Map<string, string[]>();
    admissionSet: Set<string> = new Set<string>();
    patientCounter = 1;

    constructor(filename: string) {
        super(filename);
        this.diseaseLoaded = this.getAllDiseases();
        this.parameters = Parameters.getAll();
    }

    public async all(): Promise<void> {
        await this.diseaseLoaded;
        let index = 0;
        for (const disease of this.diseasesMap) {
            index++;
            console.log('Loading disease ' + disease[1][0] + ', number ' + index + ' of total ' + this.diseases.length);
            const totalPatients = await AllPatientsForDisease.getCount(disease[0]);
            if (totalPatients !== 0) {
                const patients = await AllAdmissionsForDisease.get(disease[0]);
                await this.extractPatients(patients, this.patientExporter);
            }
        }
        console.log('Extracting patients finished.');
    }

    public async extractPatients(patients: any[], patientExporter: PatientExporter) {
        for (const patient of patients) {
            await this.extractPatient(patient, patientExporter);
        }
    }

    private async extractPatient(patient: any, patientExporter: PatientExporter) {
        for (const admission of patient.admissions) {
            if (!this.admissionSet.has(patient.id + admission.id)) {
                this.admissionSet.add(patient.id + admission.id);
                if (ExtractPatients.usePatient(admission)) {
                    const parameters = await this.getParametersMap(admission);
                    if (parameters.size >= config.mimicOptions.minCountParameters) {
                        const parsedParameters = [];
                        for (const parameter of parameters.values()) {
                            let total = 0;
                            let count = 0;
                            let isNumber = true;
                            let min = Infinity;
                            let max = - Infinity;
                            for (const paramElement of parameter) {
                                if (paramElement.numericalValue) {
                                    total += paramElement.numericalValue;
                                    count++;
                                    if (min > paramElement.numericalValue){
                                        min = paramElement.numericalValue;
                                    }
                                    if (max < paramElement.numericalValue) {
                                        max = paramElement.numericalValue;
                                    }
                                } else {
                                    isNumber = false;
                                }
                            }
                            if (isNumber) {
                                const averageValue = total / count;
                                parsedParameters.push({
                                    code: parameter[0].loinc,
                                    valueUnit: parameter[0].unit,
                                    values: {
                                        min,
                                        avg: averageValue,
                                        max: max
                                    }
                                });
                            }
                        }
                        const icd10Codes = await DiseasesForAdmission.get(admission.id, patient.id);
                        const diseases = [];
                        for (const code of icd10Codes) {
                            const diseaseDescriptions = this.matchICD(code, 10);
                            if (diseaseDescriptions) {
                                for (const diseaseDescription of diseaseDescriptions) {
                                    diseases.push({
                                        description: diseaseDescription,
                                        icd10: code
                                    });
                                }
                            }
                        }
                        const convertedAge = ExtractPatients.shiftAge(patient.anchorAge, patient.anchorYear, admission.admissionTime, admission.dischargeTime);
                        const newPatient = new Patient(patient.id, [], parsedParameters, [],
                            diseases, patient.gender, convertedAge, 'Patient_' + this.patientCounter);
                        this.patientCounter ++;
                        patientExporter.exportPatient(newPatient);
                    }
                }
            }
        }
    }

    private async getParametersMap(admission: any): Promise<Map<string, any[]>> {
        const parameters = new Map<string, any[]>();
        for (const parameter of admission.parameters) {
            if (parameter.loinc !== null) {
                if (await this.matchParameter(parameter.loinc)) {
                    if (parameters.has(parameter.loinc)) {
                        parameters.get(parameter.loinc)?.push(parameter);
                    } else {
                        parameters.set(parameter.loinc, [parameter]);
                    }
                }
            }
        }
        return parameters;
    }

    private static usePatient(admission: any): boolean {
        if (!config.mimicOptions.diedPatients) {
            if (admission.died) {
                return false;
            }
        }
        let result = false;
        for (const type of config.mimicOptions.admissionTypes) {
            if (type === admission.admissionType) {
                result = true;
            }
        }
        return result;
    }

    private static shiftAge(anchorAge: number, anchorYear: number, admittime: Date, dischtime: Date): number {
        const timespan = (dischtime.getTime() - admittime.getTime()) / 2;
        const toDate = new Date(admittime.getTime() + timespan);
        const fromDate = new Date(anchorYear, 0);
        const yearsBetween = (toDate.getTime() - fromDate.getTime()) / 31536000000;
        const ageInYears = anchorAge + yearsBetween;
        return Math.floor(UnitConverter.convertAgeToHours(ageInYears, 'years'));
    }

    private matchICD(code: string, version: number): string[] | undefined {
        if (version === 9) {
            console.log('Wrong icd version');
            return undefined;
        } else if (version === 10) {
            return this.diseasesMap.get(code);
        } else {
            console.error("ICD version not 9 or 10");
            return undefined;
        }
    }

    private async matchParameter(loinc: string): Promise<boolean> {
        const found = (await this.parameters).find((parameter) => {
            return parameter.loinc === loinc;
        });
        return found;
    }

    private async getAllDiseases() {
        const diseases: any[] = await Diseases.getPathDiseases();
        const result = [];
        for (const disease of diseases) {
            if (disease.icd10) {
                const code = disease.icd10.replace('.', '');
                result.push({
                    description: disease.description,
                    code
                });
            }
        }
        const map = await Diseases.getPathDiseasesMap();
        const diseasesMap = new Map();
        for (const disease of map) {
            // filter out icd codes with invalid characters
            if (IcdFilter.isValid(disease[0])) {
                diseasesMap.set(IcdFilter.replacePoints(disease[0]), disease[1]);
            }
        }
        this.diseasesMap = diseasesMap;

        this.diseases = result;
    }
}