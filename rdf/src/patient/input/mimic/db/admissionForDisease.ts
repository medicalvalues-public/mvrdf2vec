import {pool} from "./connection";

export class AdmissionForDisease {
    public static async get(icd10: string): Promise<any[]> {
        return new Promise((resolve => {
            pool.query(`SELECT diag.hadm_id, adm.subject_id, admittime, dischtime, admission_type, ethnicity 
                    FROM mimic_core.admissions as adm, mimic_hosp.diagnoses_icd as diag WHERE diag.icd_version = 10 
                    AND diag.icd_code = ($1) AND diag.subject_id = adm.subject_id AND diag.hadm_id = adm.hadm_id`,
                [icd10],
                (error: Error, result) => {
                    if (error) {
                        throw error;
                    }
                    const admissions: any = [];
                    for (const row of result.rows) {
                        admissions.push({
                            id: row.hadm_id,
                            patientId: row.subject_id,
                            admissionTime: row.admittime,
                            dischargeTime: row.dischtime,
                            admissionType: row.admission_type,
                            ethnicity: row.ethnicity
                        });
                    }
                    resolve(admissions);
                })
        }))
    }
}
