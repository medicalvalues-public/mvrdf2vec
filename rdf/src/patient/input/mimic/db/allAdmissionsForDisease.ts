import {pool} from "./connection";

export class AllAdmissionsForDisease {
    public static async get(icd10: string): Promise<any[]> {
        return new Promise((resolve => {
            pool.query(`SELECT pat.gender, pat.anchor_age, pat.anchor_year, diag.hadm_id, adm.subject_id, 
                    admittime, dischtime, admission_type, adm.hospital_expire_flag, ethnicity, lab.charttime, lab.storetime, value, valuenum, 
                    valueuom, flag, comments, loinc_code, label, category, fluid, ref_range_lower, ref_range_upper
                     FROM mimic_core.patients as 
                    pat, mimic_core.admissions as adm, mimic_hosp.diagnoses_icd as diag, mimic_hosp.labevents as lab,
                     mimic_hosp.d_labitems as dlab WHERE diag.icd_version = 10 AND diag.icd_code = ($1) AND 
                       diag.subject_id = adm.subject_id AND diag.hadm_id = adm.hadm_id AND lab.subject_id = adm.subject_id 
                       AND lab.hadm_id = adm.hadm_id AND pat.subject_id = adm.subject_id AND dlab.itemid = lab.itemid 
                       ORDER BY subject_id, hadm_id`,
                [icd10],
                (error: Error, result) => {
                    if (error) {
                        throw error;
                    }
                    const allPatients = [];
                    if (result.rows.length > 0) {
                        let currentAdmission: any = AllAdmissionsForDisease.extractAdmission(result.rows[0]);
                        let currentPatient: any = AllAdmissionsForDisease.extractPatient(result.rows[0]);
                        for (const row of result.rows) {
                            const param: any = AllAdmissionsForDisease.extractParam(row);
                            if (row.subject_id === currentPatient.id) {
                                if (row.hadm_id === currentAdmission.id) {
                                } else {
                                    currentPatient.admissions.push(currentAdmission);
                                    currentAdmission = AllAdmissionsForDisease.extractAdmission(row);
                                }
                                currentAdmission.parameters.push(param);
                            } else {
                                currentPatient.admissions.push(currentAdmission);
                                currentAdmission = AllAdmissionsForDisease.extractAdmission(row);
                                allPatients.push(currentPatient);
                                currentPatient = AllAdmissionsForDisease.extractPatient(row);
                            }
                        }
                    }
                    resolve(allPatients);
                })
        }))
    }

    private static extractAdmission(row: any) {
        return {
            id: row.hadm_id,
            patientId: row.subject_id,
            admissionTime: row.admittime,
            dischargeTime: row.dischtime,
            admissionType: row.admission_type,
            ethnicity: row.ethnicity,
            died: row.hospital_expire_flag === 1,
            parameters: []
        }
    }

    private static extractPatient(row: any) {
        return {
            id: row.subject_id,
            gender: row.gender === 'M' ? 'male' : 'female',
            anchorAge: row.anchor_age,
            anchorYear: row.anchor_year,
            admissions: []
        };
    }

    private static extractParam(row: any) {
        return {
            loinc: row.loinc_code,
            label: row.label,
            category: row.category,
            fluid: row.fluid,
            charttime: row.charttime,
            storetime: row.storetime,
            value: row.value,
            numericalValue: row.valuenum,
            unit: row.valueuom,
            flag: row.flag,
            comments: row.comments,
            lowerRange: row.ref_range_lower,
            higherRange: row.ref_range_upper
        }
    }

}
