import {pool} from "./connection";

export class LabItems {
    public static async get(patientId: number, admissionId: number): Promise<any> {
        return new Promise((resolve => {
            pool.query(`SELECT loinc_code, label, category, fluid, charttime, storetime, value, valuenum,
                    valueuom, flag, priority, comments, ref_range_lower, ref_range_upper FROM mimic_hosp.d_labitems as def, 
                    mimic_hosp.labevents as lab WHERE lab.itemid = def.itemid AND lab.subject_id = ($1) AND
                    lab.hadm_id = ($2)`,
                [patientId, admissionId],
                (error: Error, result) => {
                    if (error) {
                        throw error;
                    }
                    const parameters: any = [];
                    for (const row of result.rows) {
                        parameters.push({
                            loinc: row.loinc_code,
                            label: row.label,
                            category: row.category,
                            fluid: row.fluid,
                            charttime: row.charttime,
                            storetime: row.storetime,
                            value: row.value,
                            numericalValue: row.valuenum,
                            unit: row.valueuom,
                            flag: row.flag,
                            priority: row.priority,
                            comments: row.comments,
                            lowerRange: row.ref_range_lower,
                            higherRange: row.ref_range_upper
                        });
                    }
                    resolve(parameters);
                })
        }))
    }
}