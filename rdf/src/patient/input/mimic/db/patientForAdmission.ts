import {pool} from "./connection";

export class PatientForAdmission {
    public static async get(patientId: number): Promise<any[]> {
        return new Promise((resolve => {
            pool.query(`SELECT * FROM mimic_core.patients WHERE subject_id = ($1)`,
                [patientId],
                (error: Error, result) => {
                if (error) {
                    throw error;
                }
                const patients: any = [];
                for (const row of result.rows) {
                    patients.push({
                        id: row.subject_id,
                        gender: row.gender === 'M' ? 'male' : 'female',
                        anchorAge: row.anchor_age,
                        anchorYear: row.anchor_year
                    });
                }
                resolve(patients);
            })
        }))
    }
}