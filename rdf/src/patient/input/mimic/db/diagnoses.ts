import {pool} from "./connection";

export class Diagnoses {
    public static get(patientId: number, admissionId: number): Promise<any[]> {
        return new Promise((resolve => {
            pool.query(`SELECT def.icd_code, def.icd_version, long_title FROM mimic_hosp.d_icd_diagnoses as 
                    def, mimic_hosp.diagnoses_icd as diag WHERE subject_id = ($1) AND hadm_id = ($2) AND
                    def.icd_code = diag.icd_code AND def.icd_version = def.icd_version`,
                [patientId, admissionId],
                (error: Error, result) => {
                    if (error) {
                        throw error;
                    }
                    const diagnoses: any = [];
                    for (const row of result.rows) {
                        diagnoses.push({
                            description: row.long_title,
                            icd: row.icd_code,
                            icdVersion: row.icd_version
                        });
                    }
                    resolve(diagnoses);
                })
        }))
    }
}