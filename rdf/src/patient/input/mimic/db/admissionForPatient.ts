import {pool} from "./connection";

export class AdmissionForPatient {
    public static async exec(patientId: number): Promise<any[]> {
        return new Promise((resolve => {
            pool.query(`SELECT hadm_id, admittime, dischtime, admission_type, ethnicity 
                    FROM mimic_core.admissions WHERE subject_id = ($1)`, [patientId],
                (error: Error, result) => {
                if (error) {
                    throw error;
                }
                const admissions: any = [];
                for (const row of result.rows) {
                    admissions.push({
                        id: row.hadm_id,
                        admissionTime: row.admittime,
                        dischargeTime: row.dischtime,
                        admissionType: row.admission_type,
                        ethnicity: row.ethnicity
                    });
                }
                resolve(admissions);
            })
        }))
    }
}