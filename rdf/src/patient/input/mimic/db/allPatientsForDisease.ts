import {pool} from "./connection";

export class AllPatientsForDisease {
    public static async get(icd10: string): Promise<any[]> {
        return new Promise((resolve => {
            pool.query(`SELECT pat.gender, pat.anchor_age, pat.anchor_year, diag.hadm_id, adm.subject_id, 
                    admittime, dischtime, admission_type, ethnicity FROM mimic_core.patients as pat, mimic_core.admissions 
                    as adm, mimic_hosp.diagnoses_icd as diag WHERE diag.icd_version = 10 AND diag.icd_code = ($1) AND 
                       diag.subject_id = adm.subject_id AND diag.hadm_id = adm.hadm_id 
                       AND pat.subject_id = adm.subject_id 
                       ORDER BY subject_id, hadm_id`,
                [icd10],
                (error: Error, result) => {
                    if (error) {
                        throw error;
                    }
                    const allPatients = [];
                    if (result.rows.length > 0) {
                        let currentAdmission: any = AllPatientsForDisease.extractAdmission(result.rows[0]);
                        let currentPatient: any = AllPatientsForDisease.extractPatient(result.rows[0]);
                        for (const row of result.rows) {
                            if (row.subject_id === currentPatient.id) {
                                if (row.hadm_id === currentAdmission.id) {
                                } else {
                                    currentPatient.admissions.push(currentAdmission);
                                    currentAdmission = AllPatientsForDisease.extractAdmission(row);
                                }
                            } else {
                                currentPatient.admissions.push(currentAdmission);
                                currentAdmission = AllPatientsForDisease.extractAdmission(row);
                                allPatients.push(currentPatient);
                                currentPatient = AllPatientsForDisease.extractPatient(row);
                            }
                        }
                    }
                    resolve(allPatients);
                });
        }));
    }

    public static getCount(icd10: string): Promise<number> {
        return new Promise((resolve => {
            pool.query(`SELECT count(*) FROM mimic_core.patients as pat, mimic_core.admissions 
                    as adm, mimic_hosp.diagnoses_icd as diag WHERE diag.icd_version = 10 AND diag.icd_code = ($1) AND 
                       diag.subject_id = adm.subject_id AND diag.hadm_id = adm.hadm_id 
                       AND pat.subject_id = adm.subject_id `,
                [icd10],
                (error: Error, result) => {
                    if (error) {
                        throw error;
                    }
                    resolve(parseInt(result.rows[0].count));
                });
        }));
    }

    private static extractAdmission(row: any) {
        return {
            id: row.hadm_id,
            patientId: row.subject_id,
            admissionTime: row.admittime,
            dischargeTime: row.dischtime,
            admissionType: row.admission_type,
            ethnicity: row.ethnicity,
            parameters: []
        }
    }

    private static extractPatient(row: any) {
        return {
            id: row.subject_id,
            gender: row.gender === 'M' ? 'male' : 'female',
            anchorAge: row.anchor_age,
            anchorYear: row.anchor_year,
            admissions: []
        };
    }
}