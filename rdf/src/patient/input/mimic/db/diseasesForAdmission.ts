import {pool} from "./connection";

export class DiseasesForAdmission {
    public static async get(admissionId: number, subjectId: number): Promise<any[]> {
        return new Promise((resolve => {
            pool.query(`SELECT icd_code FROM mimic_hosp.diagnoses_icd WHERE subject_id = ($1) AND 
                hadm_id = ($2) AND icd_version = 10`,
                [subjectId, admissionId],
                (error: Error, result) => {
                    if (error) {
                        throw error;
                    }
                    const codes: any = [];
                    for (const row of result.rows) {
                        codes.push(row.icd_code.trim());
                    }
                    resolve(codes);
                })
        }))
    }
}