export const query = `
query MyQuery {
            diseases {
                nodes {
                    description
                    id
                    icd10
                    testdata {
                        nodes {
                            description
                            id
                            type
                            patientAge
                            patientAgeUnit
                            patientGender
                            patientEthnicity
                            testdataelementsByTestdataId {
                                nodes {
                                    id
                                    loinc
                                    nodeId
                                    option
                                    radlex
                                    snomed
                                    unit
                                    value
                                }
                            }
                        }
                    }
                }
            }
        }`