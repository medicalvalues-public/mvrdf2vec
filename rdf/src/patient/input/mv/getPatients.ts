import {Request} from "../../../network/request";
import {query} from "./query";
import {Patient} from "../../patient";
import {UnitConverter} from "../../../services/unitCoverter";
import {ExportPatient} from "../exportPatient";

export class GetPatients extends ExportPatient {
    patientCounter = 1;

    constructor(filename: string) {
        super(filename);
    }


    public async all(): Promise<void> {
        const patients = await Request.exec(query)
        for (const disease of patients.diseases.nodes) {
            const parsedDisease = {
                description: disease.description,
                mvId: disease.id,
                icd10: disease.icd10
            }
            for (const data of disease.testdata.nodes) {
                this.patientExporter.exportPatient(this.parseData(data, parsedDisease));
            }
        }
    }

    /**
     * Parses json string to array of observations
     * @param patient
     * @param disease
     */
    private parseData(patient: any, disease: any): Patient {
        const convertedAge = UnitConverter.convertAgeUnitToHours(patient.patientAge, patient.patientAgeUnit)
        const imagingResults: any[] = [];
        const parameters: any[] = [];
        const findings: any[] = [];
        patient.testdataelementsByTestdataId.nodes.forEach((element: any) => {
            switch (element.option) {
                case 'ImagingResult':
                    if (element.value && element.snomed && element.snomed !== "0") {
                        imagingResults.push({
                            hasImagingResult: element.value === 'true',
                            code: element.snomed,
                            codeType: 'snomed',
                            category: 'imaging'
                        });
                    }
                    break;
                case 'Parameter':
                    if (element.value && element.loinc && element.loinc !== '0' && element.value !== 'null') {
                        if (element.unit === 'YES/NO' && (element.value === '1' || element.value === '0')) {
                            element.value === '1' ? element.value = 'true' : element.value = 'false';
                        }
                        parameters.push({
                            code: element.loinc ? element.loinc : element.snomed,
                            codeType: element.loinc ? 'loinc' : 'snomed',
                            values: {
                                min: element.value,
                                avg: element.value,
                                max: element.value
                            },
                            valueUnit: element.unit,
                            category: 'laboratory'
                        });
                    }
                    break;
                case 'Finding':
                    if (element.snomed && element.snomed !== '0') {
                        findings.push({
                            code: element.snomed,
                            codeType: 'snomed',
                            hasFinding: element.value === 'true',
                            datetime: new Date(Date.now()).toISOString()
                        });
                    }
                    break;
            }
        });
        this.patientCounter++;
        return new Patient(patient.id, findings, parameters, imagingResults, [disease], patient.patientGender, convertedAge, 'Patient_' + this.patientCounter);
    }

}