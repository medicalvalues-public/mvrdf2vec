import {PatientExporter} from "../patientExporter";

export abstract class ExportPatient {
    patientExporter: PatientExporter;

    protected constructor(filenamePatients: string) {
        this.patientExporter = new PatientExporter(filenamePatients);
    }

    abstract all(): Promise<void>;
}