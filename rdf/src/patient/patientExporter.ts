import {Patient} from "./patient";
import {Exporter} from "../rdfExport/exporter";

export class PatientExporter {
    exporter: Exporter;

    constructor(filename: string) {
        this.exporter = new Exporter(filename);
        const firstLine =  'description,gender,age,category,loinc,unit,value,diseases\n'
        this.exporter.addLine(firstLine);
    }

    public export(patients: Patient[]) {
        for (const patient of patients) {
            this.exportPatient(patient);
        }
    }

    public exportPatient(patient: Patient) {
        let diseaseLine = '';
        let count = 0;
        for (const disease of patient.diseases) {
            diseaseLine += disease.icd10 + '$' + disease.description;
            count ++;
            if (count < patient.diseases.length) {
                diseaseLine += '|';
            }
        }
        const firstLine = patient.description + ',' + patient.gender + ',' + patient.age + ',,,,,' + diseaseLine + '\n';
        this.exporter.addLine(firstLine);
        for (const param of patient.parameters) {
            const line = ',,,laboratory,' + param.code + ',' + param.valueUnit + ',' + param.values.min + '|' + param.values.avg + '|' + param.values.max + ',\n';
            this.exporter.addLine(line)
        }
        for (const finding of patient.findings) {
            const line = ',,,examination,' + finding.code + ',,' + finding.hasFinding + ',\n';
            this.exporter.addLine(line)
        }
        for (const imagingResult of patient.imagingResults) {
            const line = ',,,imaging,' + imagingResult.code + ',,' + imagingResult.hasImagingResult + ',\n';
            this.exporter.addLine(line)
        }
    }
}
