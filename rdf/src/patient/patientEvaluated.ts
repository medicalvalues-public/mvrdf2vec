import {Graph} from "../graph/graph";
import {Triple} from "../graph/models/triple";
import {Patient} from "./patient";
import {Exporter} from "../rdfExport/exporter";
import {ExportInterface} from "../rdfExport/export.interface";
import {PatientsRiskFactorExporter} from "./patientsRiskFactorExporter";

export class PatientEvaluated {
    patient: Patient;
    id: string;
    gender: string;
    ageInHours: number;
    evaluatedParameters: any[];
    evaluatedFindings: any[];
    evaluatedImaging: any[];
    evaluatedDiseases: any[];

    constructor(patient: Patient, evaluatedParameters: any[], evaluatedFindings: any[],
                evaluatedImaging: any[], evaluatedDiseases: any[]) {
        this.patient = patient;
        this.id = patient.description;
        this.gender = patient.gender;
        this.ageInHours = patient.age;
        this.evaluatedParameters = evaluatedParameters;
        this.evaluatedFindings = evaluatedFindings;
        this.evaluatedDiseases = evaluatedDiseases;
        this.evaluatedImaging = evaluatedImaging;
    }

    public export(exporter: Exporter) {
        let diseases = '';
        for (const disease of this.evaluatedDiseases) {
            diseases += disease.icd10 + '$' + disease.description + '|';
        }
        diseases = diseases.substring(0, diseases.length - 1);
        const firstLine = this.patient.id + ',' + this.patient.gender + ',' + this.patient.age + ',,,,,,' + diseases + '\n';
        exporter.addLine(firstLine);
        for (const param of this.evaluatedParameters) {
            const line = ',,,laboratory,' + param.loinc + ',' + param.valueUnit + ',' + param.value + ',' + param.evaluation + ',\n';
            exporter.addLine(line);
        }
        for (const finding of this.evaluatedFindings) {
            const line = ',,,examination,' + finding.code + ',,' + finding.hasCondition + ',' + finding.hasCondition + ',\n';
            exporter.addLine(line);
        }
        for (const imagingResult of this.evaluatedImaging) {
            const line = ',,,imaging,' + imagingResult.code + ',,' + imagingResult.hasImagingResult +
                ',' + imagingResult.hasImagingResult + ',\n';
            exporter.addLine(line);
        }
    }

    public async exportToGraph(graph: Graph, exporter: ExportInterface, patientsRiskFactorExporter: PatientsRiskFactorExporter) {
        for (const parameter of this.evaluatedParameters) {
            const pathologicalRangeTriples = graph.pathologicalRanges.parameterInPathologicalRanges(parameter.loinc, parameter.value, parameter.valueUnit, this.patient);
            for (const triple of pathologicalRangeTriples) {
                await exporter.export(triple);
            }
            const description = graph.parameters.getDescription(parameter.code);
            if (description) {
                if (parameter.evaluation === 'increased' || parameter.evaluation === 'decreased') {
                    // add not normal values
                    const triple = new Triple(this.id, description + '_' + 'not_normal', 'has');
                    patientsRiskFactorExporter.add(triple, 'parameter');
                    await exporter.export(triple);
                    if (parameter.evaluation === 'increased') {
                        const triple1 = new Triple(this.id, description + '_' + 'not_decreased', 'has');
                        patientsRiskFactorExporter.add(triple1, 'parameter');
                        await exporter.export(triple1);
                    } else {
                        const triple1 = new Triple(this.id, description + '_' + 'not_increased', 'has');
                        patientsRiskFactorExporter.add(triple1, 'parameter');
                        await exporter.export(triple1);
                    }
                } else if (parameter.evaluation !== 'yes' && parameter.evaluation !== 'no') {
                    const triple = new Triple(this.id, description + '_' + 'not_increased', 'has');
                    const triple1 = new Triple(this.id, description + '_' + 'not_decreased', 'has');
                    await exporter.export(triple);
                    patientsRiskFactorExporter.add(triple, 'parameter');
                    await exporter.export(triple1);
                    patientsRiskFactorExporter.add(triple1, 'parameter');
                }
                const triple2 = new Triple(this.id, description + '_' + parameter.evaluation, 'has');
                await exporter.export(triple2);
                patientsRiskFactorExporter.add(triple2, 'parameter');
            }
        }
        for (const finding of this.evaluatedFindings) {
            const description = graph.findings.getDescription(finding.code);
            if (description) {
                let triple;
                if (finding.hasFinding) {
                    triple = new Triple(this.id, description + '_' + 'currently_examined_with', 'has');
                } else {
                    triple = new Triple(this.id, description + '_' + 'currently_not_examined_with', 'has');
                }
                await exporter.export(triple);
                patientsRiskFactorExporter.add(triple, 'finding');
            }
        }
        for (const imagingResult of this.evaluatedImaging) {
            const description = graph.imagingResults.getDescription(imagingResult.code);
            if (description) {
                let triple;
                if (imagingResult.hasImagingResult) {
                    triple = new Triple(this.id, description + '_' + 'currently_examined_with', 'has');
                } else {
                    triple = new Triple(this.id, description + '_' + 'currently_not_examined_with', 'has')
                }
                await exporter.export(triple);
                patientsRiskFactorExporter.add(triple, 'imagingResult');
            }
        }
        for (const disease of this.evaluatedDiseases) {
            const triple = new Triple(this.id, disease.description + '_' + 'currently_examined_with', 'suffers_from');
            await exporter.export(triple);
        }
        const triples = graph.patientBaseData.patientInPatientBaseData(this.patient);
        for (const triple of triples) {
            await exporter.export(triple);
            patientsRiskFactorExporter.add(triple, 'patientBaseData');
        }
    }


}
