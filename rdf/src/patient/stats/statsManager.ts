import path from "path";
import fs from "fs";
import * as Papa from "papaparse";

export class StatsManager {
    findings = [];
    parameters = [];
    imagingResults = [];

    constructor(filename: string) {
        this.parseFiles(path.resolve(__dirname, '../../' + filename));
    }

    parseFiles(path: string): void {
        const findingsString = fs.readFileSync(path + '/findings.csv').toString();
        const findingsParsed: any = Papa.parse(findingsString, {header: true, skipEmptyLines: true});
        this.findings = findingsParsed.data;
        const paramsString = fs.readFileSync(path + '/parameters.csv').toString();
        const parametersParsed: any = Papa.parse(paramsString, {header: true, skipEmptyLines: true});
        this.parameters = parametersParsed.data;
        const imagingResultsString = fs.readFileSync(path + '/imaging_results.csv').toString();
        const imagingResultsParsed: any = Papa.parse(imagingResultsString, {header: true, skipEmptyLines: true});
        this.imagingResults = imagingResultsParsed.data;
    }

    public getParameterCount(loinc: string): number {
        const found: any = this.parameters.find((param: any) => param.loinc === loinc);
        if (found) {
            return parseInt(found.total_valid);
        }
        return 0;
    }
}