import {Exporter} from "../../rdfExport/exporter";
import config from "../../config";
const fs = require('fs');
const path = require('path');

export class GlobalStats {
    parameterExporter: Exporter;
    diseaseExporter: Exporter;
    findingExporter: Exporter;
    imagingResultExporter: Exporter;
    generalExporter: Exporter;
    totalParameters = 0;
    parameters: Map<string, any> = new Map<string, any>();
    totalFindings = 0
    findings: Map<string, any> = new Map<string, any>();
    totalImagingResults = 0;
    imagingResults: Map<string, any> = new Map<string, any>();
    totalPatients = 0;
    diseases: Map<string, number> = new Map<string, number>();

    constructor() {
        const absolutePath = path.resolve(__dirname, '../../' + config.output.statsDirectory);
        if (!fs.existsSync(absolutePath)) {
            fs.mkdirSync(absolutePath);
        }
        this.parameterExporter = new Exporter(config.output.statsDirectory + '/parameters.csv');
        this.diseaseExporter = new Exporter(config.output.statsDirectory + '/diseases.csv');
        this.findingExporter = new Exporter(config.output.statsDirectory + '/findings.csv');
        this.imagingResultExporter = new Exporter(config.output.statsDirectory + '/imaging_results.csv');
        this.generalExporter = new Exporter(config.output.statsDirectory + '/stats.txt');
    }

    public addParameter(loinc: string, evaluation: string) {
        this.totalParameters ++;
        if (this.parameters.has(loinc)) {
            const value = this.parameters.get(loinc);
            if (value) {
                value[evaluation] = value[evaluation] + 1;
            } else {
                this.parameters.set(loinc, {
                    normal: 0,
                    increased: 0,
                    decreased: 0,
                    no_reference_range: 0,
                    unit_conversion_error: 0,
                    yes: 0,
                    no: 0
                });
            }
        } else {
            const newParam: any = {
                normal: 0,
                increased: 0,
                decreased: 0,
                no_reference_range: 0,
                unit_conversion_error: 0,
                yes: 0,
                no: 0
            };
            newParam[evaluation] = 1;
            this.parameters.set(loinc, newParam);
        }
    }

    public addFinding(snomed: string, hasFinding: boolean) {
        this.totalFindings++;
        this.addToMap(this.findings, snomed, hasFinding);
    }

    public addImagingResult(snomed: string, hasImagingResult: boolean) {
        this.addToMap(this.imagingResults, snomed, hasImagingResult);
    }

    public addToMap(map: Map<string, any>, key: string, evaluation: boolean): void {
        if (map.has(key)) {
            const count = map.get(key);
            if (count) {
                const id = evaluation ? 'available' : 'not_available'
                count[id] = count[id] + 1;
            } else {
                map.set(key, {
                    available: 0,
                    not_available: 0
                });
            }
        } else {
            map.set(key, {
                available: 0,
                not_available: 0
            });
        }
    }

    public addPatient() {
        this.totalPatients ++;
    }

    public addDisease(icd10: string) {
        const disease = this.diseases.get(icd10);
        if (disease) {
            this.diseases.set(icd10, disease + 1);
        } else {
            this.diseases.set(icd10, 1);
        }
    }

    public export() {
        const firstLine = 'loinc,normal,increased,decreased,yes,no,no_reference_range,conversion_error,total_valid,total\n';
        this.parameterExporter.addLine(firstLine);
        for (const parameter of this.parameters) {
            const totalValid = parameter[1].normal + parameter[1].increased + parameter[1].decreased + parameter[1].yes +
            parameter[1].no;
            const total = totalValid + parameter[1].no_reference_range + parameter[1].unit_conversion_error;
            const line = parameter[0] + ',' + parameter[1].normal + ',' + parameter[1].increased + ',' +
                parameter[1].decreased + ',' + parameter[1].yes + ',' + parameter[1].no + ','
                + parameter[1].no_reference_range + ',' + parameter[1].unit_conversion_error + ',' + totalValid + ',' + total + '\n';
            this.parameterExporter.addLine(line);
        }
        const diseaseFirstLine = 'icd10,total\n';
        this.diseaseExporter.addLine(diseaseFirstLine);
        for (const disease of this.diseases) {
            const line = disease[0] + ',' + disease[1] + '\n';
            this.diseaseExporter.addLine(line);
        }
        const findingFirstLine = 'snomed,available,not_available,total\n';
        this.findingExporter.addLine(findingFirstLine);
        for (const finding of this.findings) {
            const total = finding[1].available + finding[1].not_available;
            const line = finding[0] + ',' + finding[1].available + ',' + finding[1].not_available + ',' + total + '\n';
            this.findingExporter.addLine(line);
        }
        this.imagingResultExporter.addLine(findingFirstLine);
        for (const imagingResult of this.imagingResults) {
            const total = imagingResult[1].available + imagingResult[1].not_available;
            const line = imagingResult[0] + ',' + imagingResult[1].available + ',' + imagingResult[1].not_available +
                ',' + total + '\n';
            this.imagingResultExporter.addLine(line);
        }
        this.generalExporter.addLine('Total Patients: ' + this.totalPatients);
    }
}