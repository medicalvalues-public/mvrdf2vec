import {Patient} from "./patient";
import {ReasoningRequest} from "../reasoning/reasoningRequest";
import {PatientEvaluated} from "./patientEvaluated";
import {GlobalStats} from "./stats/globalStats";

export class ProcessPatient {
    public static async exec(patient: Patient, stats: GlobalStats): Promise<PatientEvaluated> {
        const evaluatedParameters = [];
        stats.addPatient();
        for (const finding of patient.findings) {
            stats.addFinding(finding.code, finding.hasCondition);
        }
        for (const imagingResult of patient.imagingResults) {
            stats.addImagingResult(imagingResult.code, imagingResult.hasImagingResult);
        }
        for (const disease of patient.diseases) {
            stats.addDisease(disease.icd10);
        }
        for (const parameter of patient.parameters) {
            let evaluation: any = {};
            const body = {
                patient: {
                    id: patient.id,
                    gender: patient.gender,
                    convertedAge: patient.age
                },
                loinc: parameter.code,
                unit: parameter.valueUnit,
                value: '' + parameter.values.avg
            }
            let currentValue = parameter.values.avg;
            if (parameter.values.min === parameter.values.max) {
                evaluation = await ReasoningRequest.exec('evaluateParameter', body);
            } else {
                currentValue = parameter.values.min;
                body.value = parameter.values.min;
                const min: any = await ReasoningRequest.exec('evaluateParameter', body);
                if (min.evaluation !== 'no_reference_range' && min.evaluation !== 'unit_conversion_error' &&
                    min.evaluation !== 'parameter_not_found') {
                    if (min.evaluation === 'increased') {
                        evaluation = min;
                    } else {
                        currentValue = parameter.values.max;
                        body.value = parameter.values.max;
                        const max: any = await ReasoningRequest.exec('evaluateParameter', body);
                        if (max.evaluation === min.evaluation) {
                            evaluation = min;
                        } else {
                            if (min.evaluation !== 'normal' && max.evaluation === 'normal') {
                                evaluation = min;
                            } else if (min.evaluation === 'normal' && max.evaluation !== 'normal') {
                                evaluation = max;
                            } else {
                                currentValue = parameter.values.avg;
                                body.value = parameter.values.avg;
                                evaluation = await ReasoningRequest.exec('evaluateParameter', body);
                            }
                        }
                    }
                } else {
                    evaluation.evaluation = min.evaluation;
                }
            }
            if (evaluation.evaluation !== 'parameter_not_found') {
                if (evaluation.evaluation !== 'no_reference_range' && evaluation.evaluation !== 'unit_conversion_error') {
                    evaluatedParameters.push({
                        loinc: evaluation.loinc,
                        offset: evaluation.offset,
                        evaluation: evaluation.evaluation,
                        value: currentValue,
                        valueUnit: parameter.valueUnit
                    });
                }
                stats.addParameter(parameter.code, evaluation.evaluation);
            }
        }
        return new PatientEvaluated(patient, evaluatedParameters, patient.findings, patient.imagingResults, patient.diseases);
    }
}