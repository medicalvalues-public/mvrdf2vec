import {Patient} from "./patient";
import {ProcessPatient} from "./processPatient";
import {Exporter} from "../rdfExport/exporter";
import {GlobalStats} from "./stats/globalStats";
import {UnitConverter} from "../services/unitCoverter";

const fs = require('fs');
const path = require('path')
const readline = require('readline');

export class EvaluatePatientsFromFile {
    filename: string;
    exporter: Exporter;
    statistics: GlobalStats;

    constructor(inputFile: string, outputFile: string) {
        this.exporter = new Exporter(outputFile);
        const firstLine = 'name,gender,age,type,code,unit,value,evaluation,diseases\n';
        this.exporter.addLine(firstLine);
        const absolutePath = path.resolve(__dirname, '../' + inputFile);
        if (!fs.existsSync(absolutePath)) {
            throw new Error('CSV Import File does not exist');
        }
        this.filename = absolutePath;
        this.statistics = new GlobalStats();
    }

    async start() {
        const fileStream = fs.createReadStream(this.filename);

        const rl = readline.createInterface({
            input: fileStream,
            crlfDelay: Infinity
        });
        // Note: we use the crlfDelay option to recognize all instances of CR LF
        // ('\r\n') in input.txt as a single line break.
        let firstLine = true;
        let firstPatient = true;
        let name = "";
        let gender = "";
        let age = -1;
        let diseases: any[] = [];
        let parameters = [];
        let examination = [];
        let imaging = [];
        for await (const line of rl) {
            if (firstLine) {
                firstLine = false;
            } else {
                const split = line.split(',');
                if (split[0].length > 0) {
                    if (firstPatient) {
                        firstPatient = false;
                    } else {
                        const patient = new Patient(name, examination, parameters, imaging, diseases, gender, age);
                        const patientEvaluated = await ProcessPatient.exec(patient, this.statistics);
                        patientEvaluated.export(this.exporter);
                    }
                    name = split[0];
                    gender = split[1];
                    age = split[2];
                    const diseasesString = split[7].split('|');
                    const newDiseases = [];
                    for (const disease of diseasesString) {
                        const diseaseSplit = disease.split('$');
                        newDiseases.push({
                            icd10: diseaseSplit[0],
                            description: diseaseSplit[1]
                        });
                    }
                    diseases = newDiseases;
                    examination = [];
                    parameters = [];
                    imaging = [];
                } else {
                    const code = split[4]
                    const value = split[6];
                    switch (split[3]) {
                        case 'examination':
                            examination.push({
                                code,
                                hasCondition: value === 'true'
                            });
                            break;
                        case 'laboratory':
                            let unit = split[5];
                            const rawValues = value.split('|');
                            let values;
                            unit = UnitConverter.filterParameterUnits(unit);
                            if (unit !== "YES/NO") {
                                values = {
                                    min: parseFloat(rawValues[0]),
                                    avg: parseFloat(rawValues[1]),
                                    max: parseFloat(rawValues[2])
                                };
                            } else {
                                values = {
                                    min: rawValues[0],
                                    avg: rawValues[0],
                                    max: rawValues[0]
                                };
                            }
                            parameters.push({
                                code,
                                valueUnit: unit,
                                values
                            });
                            break;
                        case 'imaging':
                            imaging.push({
                                code,
                                hasImagingResult: value === 'true'
                            });
                    }
                }
            }
        }
        this.statistics.export();
    }
}