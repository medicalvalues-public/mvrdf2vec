import fs from "fs";
import readline from "readline";
import {Patient} from "./patient";
import {Graph} from "../graph/graph";
import {PatientEvaluated} from "./patientEvaluated";
import {ExportInterface} from "../rdfExport/export.interface";
import path from "path";
import {PatientsRiskFactorExporter} from "./patientsRiskFactorExporter";

export class ExportProcessedPatients {
    filename: string;

    constructor(filename: string) {
        const absolutePath = path.resolve(__dirname, '../' + filename);
        if (!fs.existsSync(absolutePath)) {
            throw new Error('CSV Import File does not exist');
        }
        this.filename = absolutePath;
    }

    public async exec(graphExporter: ExportInterface, patientsRiskFactorExporter: PatientsRiskFactorExporter, graph: Graph):
        Promise<void> {
        const fileStream = fs.createReadStream(this.filename);
        const rl = readline.createInterface({
            input: fileStream,
            crlfDelay: Infinity
        });
        let firstLine = true;
        let firstPatient = true;
        let name = '';
        let parameters = [];
        let findings: any[] = [];
        let imagingResults: any[] = [];
        let diseases: {description: string, icd10: string}[] = [];
        let gender = '';
        let age = -1;
        for await (const line of rl) {
            if (firstLine) {
                firstLine = false;
            } else {
                const split = line.split(',');
                if (split[0].length > 0) {
                    if (firstPatient) {
                        firstPatient = false;
                    } else {
                        const patient = new Patient(name, findings, parameters, imagingResults, diseases, gender, age);
                        const patientEvaluated = new PatientEvaluated(patient, parameters, findings, imagingResults, diseases);
                        await patientEvaluated.exportToGraph(graph, graphExporter, patientsRiskFactorExporter);
                        parameters = [];
                        findings = [];
                        imagingResults = []
                    }
                    name = split[0];
                    gender = split[1];
                    age = Number.parseInt(split[2]);
                    const rawDiseases = split[8].split('|');
                    const newDiseases = [];
                    for (const disease of rawDiseases) {
                        const diseaseSplit = disease.split('$');
                        newDiseases.push({
                            icd10: diseaseSplit[0],
                            description: diseaseSplit[1]
                        });
                    }
                    diseases = newDiseases;
                } else {
                    const code = split[4]
                    const value = split[6];
                    switch (split[3]) {
                        case 'examination':
                            findings.push({
                                code,
                                hasCondition: value === 'true'
                            });
                            break;
                        case 'laboratory':
                            let unit = split[5];
                            const evaluation = split[7];
                            if (unit === 'IU') {
                                unit = '[IU]';
                            }
                            parameters.push({
                                code,
                                valueUnit: unit,
                                value,
                                evaluation
                            });
                            break;
                        case 'imaging':
                            imagingResults.push({
                                code,
                                hasImagingResult: value === 'true'
                            });
                    }
                }
            }
        }

    }
}