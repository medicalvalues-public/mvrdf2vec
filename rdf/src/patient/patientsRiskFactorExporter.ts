import {Exporter} from "../rdfExport/exporter";
import {Triple} from "../graph/models/triple";

export class PatientsRiskFactorExporter {
    exporter: Exporter;

    constructor(filename: string) {
        this.exporter = new Exporter(filename);
        const firstLine = 'patient,factor,type\n';
        this.exporter.addLine(firstLine);
    }

    public add(triple: Triple, type: string): void {
        const line = triple.from + ',' + triple.to + ',' + type + '\n';
        this.exporter.addLine(line);
    }
}