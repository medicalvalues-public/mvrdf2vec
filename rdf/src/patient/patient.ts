export class Patient {
    description: string;
    id: string
    parameters: any[];
    findings: any[];
    imagingResults: any[];
    diseases: {description: string, icd10: string}[];
    gender: string;
    age: number;
    ethnicity: string | null = null;

    constructor(id: string, findings: any[], parameters: any[], imagingResults: any[], diseases:
                    {description: string, icd10: string }[],
                gender: string, age: number, description?: string) {
        this.findings = findings;
        this.parameters = parameters;
        this.imagingResults = imagingResults;
        this.diseases = diseases;
        this.gender = gender;
        this.age = age;
        this.id = id;
        this.description = description ? description :id;
    }
}
