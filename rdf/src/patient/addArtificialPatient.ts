import {Exporter} from "../rdfExport/exporter";
import * as Papa from 'papaparse';
import {Graph} from "../graph/graph";
import {ExportInterface} from "../rdfExport/export.interface";
import {Triple} from "../graph/models/triple";

const fs = require('fs');
const path = require('path');

export class AddArtificialPatient {
    graphExporter: ExportInterface;
    newPatientsExporter: Exporter;
    graph: Graph;
    patientNumber = 1;

    constructor(graphExporter: ExportInterface, outputFilePatients: string, graph: Graph) {
        this.graphExporter = graphExporter;
        this.newPatientsExporter = new Exporter(outputFilePatients);
        const firstLine = 'name,icd10,diseases\n';
        this.newPatientsExporter.addLine(firstLine);
        this.graph = graph;
    }

    public add(): void {

    }

    public async addByDisease(minCount: number, patientQuantity: number, diseasePath: string): Promise<void> {
        const absolutePath = path.resolve(__dirname, '../' + diseasePath);
        const diseaseString = fs.readFileSync(absolutePath).toString();
        const parsed: any = Papa.parse(diseaseString, {header: true, skipEmptyLines: true});
        const data: any[] = parsed.data;
        for (const row of data) {
            console.log(row);
            if (row.total > minCount) {
                const diseases = this.graph.diseases.diseases.get(row.icd10);
                if (diseases) {
                    for (let i = 0; i < patientQuantity; i++) {
                        let diseasesLine = '';
                        const patientName = 'Patient_N' + this.patientNumber;
                        for (const disease of diseases) {
                            const triple = new Triple(patientName, disease.description + '_' + 'currently_examined_with', 'suffers_from');
                            await this.graphExporter.export(triple);
                            diseasesLine += disease.description + '|';
                        }
                        diseasesLine = diseasesLine.substring(0, diseasesLine.length - 1);
                        const line = patientName + ',' + row.icd10 + ',' + diseasesLine + '\n';
                        this.newPatientsExporter.addLine(line);
                        this.patientNumber++;
                    }
                }
            }
        }
    }
}